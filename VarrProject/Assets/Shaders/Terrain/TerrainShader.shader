﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Custom/TerrainShader" {
	Properties {
		_MainTex("Base (RGB)", 2D) = "white" {}
	}
	
	Subshader {
		Pass {
			Tags { "LightMode" = "ForwardBase" }
			
			CGPROGRAM
			
			#pragma vertex   vert
			#pragma fragment frag
			
			#include"UnityCG.cginc"
			
			struct VertexInput {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 color : COLOR;
				float4 uv : TEXCOORD0;
			};
			
			struct VertexOutput {
				float4 pos : SV_POSITION;
				float4 color : COLOR;
				float4 uv : TEXCOORD0;
			};
			
			uniform float4 _LightColor0;
			
            uniform float _DaylightTime;
			sampler2D _MainTex;
			
			
			
			VertexOutput vert(VertexInput input) {
				VertexOutput output;
				
				// _Object2World is model matrix
				// _World2Object is model matrix inverse
				
				float3 normalDirection = normalize(
					mul(float4(input.normal, 0.0), unity_WorldToObject).xyz
				);
				float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
				
				//float3 diffuseReflection = _LightColor0.rgb * max(
				//    0.0, dot(normalDirection, lightDirection)
			    //);
			    
			    //float colorMapLight = 
			    
//			    output.color = (
//			    	UNITY_LIGHTMODEL_AMBIENT + (2 * float4(diffuseReflection, 1.0f))
//		    	);

                float diffuseCoef = max(0.0, dot(normalDirection, lightDirection));

				output.color = input.color;
                output.color.a = diffuseCoef;
				output.pos = UnityObjectToClipPos(input.vertex);
				output.uv = input.uv;
				
				return(output);
			}
			
			
			
			float4 frag(VertexOutput input) : COLOR {
				const float pi = 3.14159;
                float daylightTime = atan2(_WorldSpaceLightPos0.y, _WorldSpaceLightPos0.x) / pi;
                float shadowFactor = min(daylightTime - input.color.r, input.color.g - daylightTime);
                shadowFactor = step(0, shadowFactor);
                
                float diffuseFactor = min(shadowFactor, input.color.a);
                float ambientDiffuseFactor = min(shadowFactor + 0.25, input.color.a) * (1 - step(0.001, diffuseFactor));
                
                float3 diffuseReflection = min(shadowFactor, input.color.a) * _LightColor0.rgb;
                float3 diffuseAmbientReflection = ambientDiffuseFactor * UNITY_LIGHTMODEL_AMBIENT.rgb;
                
                float3 diffuseColor = diffuseReflection + diffuseAmbientReflection;
                float4 finalColor = UNITY_LIGHTMODEL_AMBIENT + (2 * float4(diffuseColor, 1.0f));
                
                //float4 finalColor = UNITY_LIGHTMODEL_AMBIENT + (2 * float4(diffuseReflection, 1.0f));
				return(finalColor * tex2D(_MainTex, input.uv));
				//return(input.color);
			}
			
			ENDCG
		}
	}
}
