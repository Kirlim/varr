// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Opaque/Grayscale Luma" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		
		Pass {
			Lighting Off
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			uniform sampler2D _MainTex;
			uniform half4 _Color;
			
			
			
			struct Input {
				half4 vertex : POSITION;
				half4 texcoord : TEXCOORD0;
			};
			
			struct V2F {
				half4 vertex : POSITION;
				half4 texcoord : TEXCOORD0;
			};
			
			
			
			V2F vert(Input In) {
				V2F Out;
				Out.vertex = UnityObjectToClipPos(In.vertex);
				Out.texcoord = In.texcoord;
				return(Out);
			}
			
			
			
			half4 frag(V2F In) : COLOR {
				const half3 luma = half3(0.2126, 0.7152, 0.0722);
				half4 finalColor = tex2D(_MainTex, In.texcoord.xy);
				finalColor.rgb *= luma.rgb;
				finalColor.rgb = finalColor.r + finalColor.g + finalColor.b;
				return(finalColor);
			}
	
			ENDCG
		}
	} 
}
