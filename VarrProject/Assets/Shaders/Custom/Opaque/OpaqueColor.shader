// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Opaque/Color" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		Pass {
			Lighting Off
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			uniform half4 _Color;
	
			
			
			struct Input {
				half4 vertex : POSITION;
			};
			
			struct V2F {
				half4 vertex : POSITION;
			};
			
			
			
			V2F vert(Input In) {
				V2F Out;
				Out.vertex = UnityObjectToClipPos(In.vertex);
				return(Out);
			}
			
			
			
			half4 frag(V2F In) : COLOR {
				return(_Color);
			}
	
			ENDCG
		}
	} 
}
