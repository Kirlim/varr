// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/Transparent/Fade Camera Distance" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Color ("Color", Color) = (1,1,1,1)
		_StartFadeDist ("Start Fade Distance", float) = 100
		_EndFadeDist ("End Fade Distance", float) = 150
	}
	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		
		Pass {
			Lighting Off
			Blend SrcAlpha OneMinusSrcAlpha
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			uniform sampler2D _MainTex;
			uniform half4 _Color;
			uniform float _StartFadeDist;
			uniform float _EndFadeDist;
	
			
			
			struct Input {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
			};
			
			struct V2F {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
				float dist : TEXCOORD1;
			};
			
			
			
			V2F vert(Input In) {
				V2F Out;
				Out.vertex = UnityObjectToClipPos(In.vertex);
				Out.texcoord = In.texcoord;
				Out.dist = distance(_WorldSpaceCameraPos, mul(unity_ObjectToWorld, In.vertex));
				return(Out);
			}
			
			
			
			half4 frag(V2F In) : COLOR {
				half a = lerp(1.0, 0.0, (In.dist - _StartFadeDist) / (_EndFadeDist - _StartFadeDist));
				half4 color = _Color * tex2D(_MainTex, In.texcoord.xy);;
				color.a = a;
				return(color);
			}
	
			ENDCG
		}
	} 
}
