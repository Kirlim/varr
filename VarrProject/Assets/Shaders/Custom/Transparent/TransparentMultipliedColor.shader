// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Transparent/Multiplied Color" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Color ("Color", Color) = (1,1,1,1)
	}
	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		
		Pass {
			Lighting Off
			Blend SrcAlpha OneMinusSrcAlpha
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			uniform sampler2D _MainTex;
			uniform half4 _Color;
	
			
			
			struct Input {
				half4 vertex : POSITION;
				half4 texcoord : TEXCOORD0;
			};
			
			struct V2F {
				half4 vertex : POSITION;
				half4 texcoord : TEXCOORD0;
			};
			
			
			
			V2F vert(Input In) {
				V2F Out;
				Out.vertex = UnityObjectToClipPos(In.vertex);
				Out.texcoord = In.texcoord;
				return(Out);
			}
			
			
			
			half4 frag(V2F In) : COLOR {
				half4 finalColor = _Color * tex2D(_MainTex, In.texcoord.xy);
				return(finalColor);
			}
	
			ENDCG
		}
	} 
}
