﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;



public class ToolsbarBehavior : MonoBehaviour {
	public PlayerInputController playerInputControllerRef;

	public Toggle tglBuildRoad;
	public Toggle tglBuildFarmerHouse;
	public Toggle tglBuildFarm;
	public Toggle tglBuildGranary;
	public Toggle tglBuildGuardsHouse;



	protected void Awake() {
		Assert.Test(playerInputControllerRef != null);

		Assert.Test(tglBuildRoad);
		Assert.Test(tglBuildFarmerHouse);
		Assert.Test(tglBuildFarm);
		Assert.Test(tglBuildGranary);
		Assert.Test(tglBuildGuardsHouse);
	}



	public void OnToggleButtonUpdate() {
		PlayerMapInputActions playerAction = PlayerMapInputActions.None;

		if(tglBuildRoad.isOn)        playerAction = PlayerMapInputActions.BuildDirtRoad;
		if(tglBuildFarmerHouse.isOn) playerAction = PlayerMapInputActions.BuildFarmersHouse;
		if(tglBuildFarm.isOn)        playerAction = PlayerMapInputActions.BuildFarm;
		if(tglBuildGranary.isOn)     playerAction = PlayerMapInputActions.BuildGranary;
		if(tglBuildGuardsHouse.isOn) playerAction = PlayerMapInputActions.BuildGuardsHouse;

		playerInputControllerRef.SwitchPlayerAction(playerAction);
	}
}
