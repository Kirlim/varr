﻿using UnityEngine;
using System.Collections;



public class TerrainCreator : MonoBehaviour {
	public GameObject terrainRoot;
	public int terrainWidth, terrainHeight;
	public float minHeight, maxHeight;
	public Material material;

	public float tileSize;

	private float[][] m_HeightMap;


	[ContextMenu("Create New Terrain")]
	public void CreateNewTerrain() {
		m_HeightMap = new float[terrainWidth+1][];
		for(int i = 0; i < m_HeightMap.Length; ++i) {
			m_HeightMap[i] = new float[terrainHeight+1];
		}

		for(int x = 0; x < terrainWidth; ++x) {
			for(int y = 0; y < terrainHeight; ++y) {
				m_HeightMap[x][y] = Random.Range(minHeight, maxHeight);
			}
		}

		int mid = terrainWidth / 2;
		for(int x = mid - 10; x <= mid + 10; ++x) {
			if(x >= 0 && x < terrainWidth + 1) {
				for(int y = 0; y < terrainHeight + 1; ++y) {
					m_HeightMap[x][y] += 1.0f * (float) Mathf.Abs(x - mid);
				}
			}
		}

		for(int x = 0; x < terrainWidth; ++x) {
			for(int y = 0; y < terrainHeight; ++y) {
				CreateTile(x, y);
			}
		}
	}



	private void CreateTile(int x, int y) {
		GameObject tileObj = new GameObject();
		tileObj.isStatic = true;
		MeshFilter meshFilter = tileObj.AddComponent<MeshFilter>();
		MeshRenderer meshRenderer = tileObj.AddComponent<MeshRenderer>();

		// Create the mesh
		Vector3[] vertices = {
			new Vector3( - tileSize*0.5f, m_HeightMap[ x ][ y ], - tileSize*0.5f),
			new Vector3( + tileSize*0.5f, m_HeightMap[x+1][ y ], - tileSize*0.5f),
			new Vector3( + tileSize*0.5f, m_HeightMap[x+1][y+1], + tileSize*0.5f),
			new Vector3( - tileSize*0.5f, m_HeightMap[ x ][y+1], + tileSize*0.5f)
		};

		Vector2[] uv = {
			new Vector2(0.0f, 0.0f),
			new Vector2(1.0f, 0.0f),
			new Vector2(1.0f, 1.0f),
			new Vector2(0.0f, 1.0f)
		};

		Vector2[] uv1 = {
			new Vector2(0.0f, 0.0f),
			new Vector2(1.0f, 0.0f),
			new Vector2(1.0f, 1.0f),
			new Vector2(0.0f, 1.0f)
		};

		Vector3[] normals = {
			new Vector3(0.0f, 1.0f, 0.0f),
			new Vector3(0.0f, 1.0f, 0.0f),
			new Vector3(0.0f, 1.0f, 0.0f),
			new Vector3(0.0f, 1.0f, 0.0f)
		};

		Color[] colors = {
			new Color(1.0f, 1.0f, 1.0f, 1.0f),
			new Color(1.0f, 1.0f, 1.0f, 1.0f),
			new Color(1.0f, 1.0f, 1.0f, 1.0f),
			new Color(1.0f, 1.0f, 1.0f, 1.0f)
		};

		int[] triangles = { 0, 2, 1, 2, 0, 3 };

		Mesh mesh = new Mesh();
		mesh.vertices = vertices;
		mesh.uv = uv;
		mesh.uv2 = uv1;
		mesh.normals = normals;
		mesh.triangles = triangles;
		mesh.colors = colors;
		;

		meshFilter.sharedMesh = mesh;
		meshRenderer.sharedMaterial = material;

		tileObj.transform.position = new Vector3(x * tileSize, 0.0f, y * tileSize);
		tileObj.transform.parent = terrainRoot.transform;
	}
}
