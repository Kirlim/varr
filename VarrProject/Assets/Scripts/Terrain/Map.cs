﻿using UnityEngine;
using System.Collections;



/// <summary>
/// Stores map information for use in runtime.
/// </summary>
public class Map : MonoBehaviour {
    // TODO : DELETE NEXT LINE
	public MapData mapToLoad;

	// Generic map settings
	[SerializeField] protected float m_TileSize;
	[SerializeField] protected TilesetData m_Tileset;
	public float TileSize { get { return(m_TileSize); } }
	public TilesetData Tileset { get { return(m_Tileset); } }

	// Current map size
	public int Width { get; protected set; }
	public int Height { get; protected set; }

	// Terrain structure
	protected float[] m_HeightMap;
	protected MapTile[] m_Tiles;

	// Regions, for mesh fragmenting
	[SerializeField] protected int m_RegionWidth;
	[SerializeField] protected int m_RegionHeight;
	protected MapRegion[][] m_Regions;

	// Helps to know if a map is, in theory, loaded
	public bool Loaded { get; protected set; }

	// Misc.
	public OctreeBase terrainOctree;
	public int octreeDepth;




	protected void Awake() {
		Loaded = false;
//
//		// Init things
//		Vector3 mapSize = new Vector3(Width * m_TileSize, 26 * m_TileSize, Height * m_TileSize);
//		Vector3 mapCenter = mapSize * 0.5f;
//		Debug.Log("Map size: " + mapSize);
//		Debug.Log("Map center: " + mapCenter);
//		terrainOctree = new Octree(new Bounds(mapCenter, mapSize), octreeDepth);
//
//		BuildMap();

        LoadMap(mapToLoad);
	}



	protected void Update() {
		for(int rx = 0; rx < m_Regions.Length; ++rx) {
			for(int ry = 0; ry < m_Regions[rx].Length; ++ry) {
				m_Regions[rx][ry].Render();
			}
		}
	}



    protected void LoadMap(MapData mapData) {
        Assert.Test(mapData != null);
        LoadTerrain(mapData.terrainData);
        LoadMapObjects(mapData);
    }



	protected void LoadTerrain(TerrainDataSO terrainDataSO) {
        Assert.Test(terrainDataSO != null);
		LoadTerrain(terrainDataSO.data);
	}

	protected void LoadTerrain(TerrainData terrainData) {
        Assert.Test(terrainData != null);

        Width = terrainData.Width;
        Height = terrainData.Height;

        m_HeightMap = terrainData.heightMap.Clone() as float[];

        m_Tiles = new MapTile[terrainData.tilesIds.Length];
        for(int i = 0; i < m_Tiles.Length; ++i) {
            MapTile tile = new MapTile();
            tile.tile = m_Tileset.GetTile(terrainData.tilesIds[i]);
            tile.tileOrientation = terrainData.tilesOrientations[i];
            tile.mapObject = null;

            m_Tiles[i] = tile;
        }

        BuildOctree();
        BuildRegions();

        Loaded = true;
    }



    protected void LoadMapObjects(MapData mapData) {
        Assert.Test(mapData != null);
        for(int i = 0; i < mapData.placedObjects.Count; ++i) {
            MapObjectPlacementData mapObject = mapData.placedObjects[i];
            Global.MapObjectsManager.Place(mapObject.id, mapObject.position, mapObject.orientation);
        }
    }



    protected void BuildRegions() {
        int regionsX = Width / m_RegionWidth;
        if(Width % m_RegionWidth > 0) regionsX++;
        
        int regionsY = Height / m_RegionHeight;
        if(Height % m_RegionHeight > 0) regionsY++;
        
        m_Regions = new MapRegion[regionsX][];
        for(int x = 0; x < regionsX; ++x) {
            m_Regions[x] = new MapRegion[regionsY];
            for(int y = 0; y < regionsY; ++y) {
                int sx = x * m_RegionWidth;                               // start x
                int ex = Mathf.Min((x + 1) * m_RegionWidth, Width) - 1; // end x
                int sy = y * m_RegionHeight;                                // start y
                int ey = Mathf.Min((y + 1) * m_RegionHeight, Height) - 1; // end y
                
                m_Regions[x][y] = new MapRegion();
                m_Regions[x][y].SetUp(this, sx, sy, ex, ey);
            }
        }
    }



    protected void BuildOctree() {
        Vector3 mapSize = new Vector3(Width * m_TileSize, 26 * m_TileSize, Height * m_TileSize);
        Vector3 mapCenter = mapSize * 0.5f;
        terrainOctree = new SimpleOctree();
        terrainOctree.Init(new Bounds(mapCenter, mapSize), octreeDepth);
    }



	protected int GetHeightArrayIndex(int x, int y) {
		return(y * (Width + 1) + x);
	}

	protected int GetTileArrayIndex(int x, int y) {
		return(y * Width + x);
	}



	public void ChangeTile(int x, int y, int tileId) {
		int regionX = x / m_RegionWidth;
		int regionY = y / m_RegionHeight;
		m_Tiles[GetTileArrayIndex(x, y)].tile = m_Tileset.GetTile(tileId);
		
        m_Regions[regionX][regionY].RefreshTileUv(x, y);
        m_Regions[regionX][regionY].RefreshUvs();
	}



	public void BuildRoad(int x, int y) {
		Assert.Test(x >= 0 && y >= 0 && x < Width && y < Height);
		int roadType = 1;

		TileData tile = m_Tileset.GetTile(roadType, TileTypes.Road, TileSubtypes.Single);
		if(tile == null) {
			Assert.Throw();
			return;
		}

		m_Tiles[GetTileArrayIndex(x, y)].tile = tile;
		RefreshRoad( x ,  y );
		RefreshRoad(x+1,  y );
		RefreshRoad( x , y+1);
		RefreshRoad(x-1,  y );
		RefreshRoad( x , y-1);
	}



	private void RefreshRoad(int x, int y) {
		if(!(x >= 0 && y >= 0 && x < Width && y < Height)) return;
		TileTypes tileType = m_Tiles[GetTileArrayIndex(x, y)].tile.TileType;

		if(tileType == TileTypes.Road) {
			// Roads that can reach this tile
			bool east = false, north = false, west = false, south = false;
			TileSubtypes subtype = TileSubtypes.Single;
			TileOrientations orientation = TileOrientations.East;

			if(x+1 < Width && m_Tiles[GetTileArrayIndex(x+1, y)].tile.TileType == TileTypes.Road) east = true;
			if(y+1 < Height && m_Tiles[GetTileArrayIndex(x, y+1)].tile.TileType == TileTypes.Road) north = true;
			if(x > 0 && m_Tiles[GetTileArrayIndex(x-1, y)].tile.TileType == TileTypes.Road) west = true;
			if(y > 0 && m_Tiles[GetTileArrayIndex(x, y-1)].tile.TileType == TileTypes.Road) south = true;

			// Single
			if(east && !north && !west && !south) {
				subtype = TileSubtypes.SingleEnd;
				orientation = TileOrientations.West;
			} else if(!east && north && !west && !south) {
				subtype = TileSubtypes.SingleEnd;
				orientation = TileOrientations.South;
			} else if(!east && !north && west && !south) {
				subtype = TileSubtypes.SingleEnd;
				orientation = TileOrientations.East;
			} else 
			if(!east && !north && !west && south) {
				subtype = TileSubtypes.SingleEnd;
				orientation = TileOrientations.North;
			} 
			// Turns
			else if(east && north && !west && !south) {
				subtype = TileSubtypes.Turn;
				orientation = TileOrientations.South;
			} else if(!east && north && west && !south) {
				subtype = TileSubtypes.Turn;
				orientation = TileOrientations.East;
			} else if(!east && !north && west && south) {
				subtype = TileSubtypes.Turn;
				orientation = TileOrientations.North;
			} else if(east && !north && !west && south) {
				subtype = TileSubtypes.Turn;
				orientation = TileOrientations.West;
			} 
			// Straights
			else if(east && !north && west && !south) {
				subtype = TileSubtypes.Straight;
				orientation = TileOrientations.East;
			} else if(!east && north && !west && south) {
				subtype = TileSubtypes.Straight;
				orientation = TileOrientations.South;
			} 
			// Intersections
			else if(east && north && !west && south) {
				subtype = TileSubtypes.Intersection;
				orientation = TileOrientations.West;
			} else if(east && north && west && !south) {
				subtype = TileSubtypes.Intersection;
				orientation = TileOrientations.South;
			} else if(!east && north && west && south) {
				subtype = TileSubtypes.Intersection;
				orientation = TileOrientations.East;
			} else if(east && !north && west && south) {
				subtype = TileSubtypes.Intersection;
				orientation = TileOrientations.North;
			} 
			// Cross
			else if(east && north && west && south){
				subtype = TileSubtypes.Cross;
				orientation = TileOrientations.East;
			}

			m_Tiles[GetTileArrayIndex(x, y)].tileOrientation = orientation;

			ChangeTile(
				x, y,
				m_Tileset.GetTile(
                    m_Tiles[GetTileArrayIndex(x, y)].tile.GroupId,
			        m_Tiles[GetTileArrayIndex(x, y)].tile.TileType,
                    subtype
                ).Id
            );
		}
	}



	public float GetVertexHeight(int x, int y) {
		Assert.Test(x >= 0 && y >= 0 && x <= Width && y <= Height);
		return(m_HeightMap[GetHeightArrayIndex(x, y)]);
	}



	public TileOrientations GetTileOrientation(int x, int y) {
		Assert.Test(x >= 0 && y >= 0 && x < Width && y < Height);
		return(m_Tiles[GetTileArrayIndex(x, y)].tileOrientation);
	}



	/// <summary>
	/// Places an structure on the map.
	/// </summary>
	public bool BuildStructure(MapObject newMapObject) {
		Assert.Test(CanPlaceBuilding(newMapObject.PosMin, newMapObject.PosMax));
		bool success = false;

		if(CanPlaceBuilding(newMapObject.PosMin, newMapObject.PosMax)) {
			for(int x = newMapObject.PosMin.x; x <= newMapObject.PosMax.x; ++x) {
				for(int y = newMapObject.PosMin.y; y <= newMapObject.PosMax.y; ++y) {
					m_Tiles[GetTileArrayIndex(x, y)].mapObject = newMapObject;
				}
			}
			success = true;
		}

		return(success);
	}



	public void RotateUv(
		Vector2 uvStart, Vector2 uvEnd, TileOrientations orientation,
		out Vector2 v0, out Vector2 v1, out Vector2 v2, out Vector2 v3
	) {
		v0 = Vector2.zero;
		v1 = Vector2.zero;
		v2 = Vector2.zero;
		v3 = Vector2.zero;

		switch(orientation) {
		case TileOrientations.East:
			v0 = new Vector2(uvStart.x, uvStart.y);
			v1 = new Vector2(uvEnd.x, uvStart.y);
			v2 = new Vector2(uvEnd.x, uvEnd.y);
			v3 = new Vector2(uvStart.x, uvEnd.y);
			break;
		case TileOrientations.North:
			v0 = new Vector2(uvStart.x, uvEnd.y);
			v1 = new Vector2(uvStart.x, uvStart.y);
			v2 = new Vector2(uvEnd.x, uvStart.y);
			v3 = new Vector2(uvEnd.x, uvEnd.y);
			break;
		case TileOrientations.West:
			v0 = new Vector2(uvEnd.x, uvEnd.y);
			v1 = new Vector2(uvStart.x, uvEnd.y);
			v2 = new Vector2(uvStart.x, uvStart.y);
			v3 = new Vector2(uvEnd.x, uvStart.y);
			break;
		case TileOrientations.South:
			v0 = new Vector2(uvEnd.x, uvStart.y);
			v1 = new Vector2(uvEnd.x, uvEnd.y);
			v2 = new Vector2(uvStart.x, uvEnd.y);
			v3 = new Vector2(uvStart.x, uvStart.y);
			break;
		}
	}



	public void DebugDraw() {
		terrainOctree.DebugDraw();
	}



	#region Tile And Position Data Fetching

	public IntVector2 GetPosAsTile(Vector3 basePosition) {
		return(new IntVector2(
			Mathf.FloorToInt(basePosition.x / m_TileSize),
			Mathf.FloorToInt(basePosition.z / m_TileSize)
		));
	}



	public bool IsFree(int x, int y) {
		Assert.Test(x >= 0 && y >= 0 && x < Width && y < Height);
		return(m_Tiles[GetTileArrayIndex(x, y)].IsFree);
	}
	


	public bool CanPlaceBuilding(IntVector2 minPos, IntVector2 maxPos) {
		if(minPos.x < 0 || minPos.y < 0 || maxPos.x < 0 || maxPos.y < 0) return(false);
		if(minPos.x >= Width || maxPos.x >= Width) return(false);
		if(minPos.y >= Height || maxPos.y >= Height) return(false);
		
		for(int x = minPos.x; x <= maxPos.x; ++x) {
			for(int y = minPos.y; y <= maxPos.y; ++y) {
                if(!IsFree(x, y)) return(false);
                if(IsRoad(x,y)) return(false);

                int regionX = x / m_RegionWidth;
                int regionY = y / m_RegionHeight;
                if(!m_Regions[regionX][regionY].CanHoldBuildings(x, y)) return(false);
			}
		}
		return(true);
	}



    public bool CanPlaceRoad(int tileX, int tileY) {
        Assert.Test(tileX >= 0 && tileY >= 0);
        Assert.Test(tileX < Width && tileY < Height);

        if(!IsFree(tileX, tileY)) return(false);
        if(IsRoad(tileX,tileY)) return(false);
        
        int regionX = tileX / m_RegionWidth;
        int regionY = tileY / m_RegionHeight;
        if(!m_Regions[regionX][regionY].CanHoldRoads(tileX, tileY)) return(false);

        return(true);
    }



	public bool IsRoad(int x, int y) {
		Assert.Test(x >= 0 && y >= 0 && x < Width && y < Height);
		return(m_Tiles[GetTileArrayIndex(x, y)].tile.TileType == TileTypes.Road);
	}



	public const string FarmTag = "Farm";
	public bool IsFarm(int x, int y) {
		Assert.Test(x >= 0 && y >= 0 && x < Width && y < Height);
		if(m_Tiles[GetTileArrayIndex(x, y)].mapObject == null) return(false);
		return(m_Tiles[GetTileArrayIndex(x, y)].mapObject.tag == FarmTag);
		// TODO : USE SOMETHING BETTER THAN "FARM" TAG
	}



	public float GetTileHeight(int x, int y) {
		Assert.Test(x >= 0 && y >= 0 && x < Width && y < Height);
		float height = (
			m_HeightMap[GetHeightArrayIndex(x, y)] + m_HeightMap[GetHeightArrayIndex(x+1, y)] +
            m_HeightMap[GetHeightArrayIndex(x, y+1)] + m_HeightMap[GetHeightArrayIndex(x+1, y+1)]
		) * 0.25f;
		return(height);
	}
	
	
	
	public int GetTileId(int x, int y) {
		Assert.Test(x >= 0 && y >= 0 && x < Width && y < Height);
		return(m_Tiles[GetTileArrayIndex(x, y)].tile.Id);
	}



    public float GetTerrainHeight(float x, float z) {
//        // Offset the position to the tiling system
//        x -= TileSize * 0.5f;
//        z -= TileSize * 0.5f;
//
//        Assert.Test(x >= -TileSize * 0.5f && x <= (Width * TileSize) - (TileSize * 0.5f));
//        Assert.Test(y >= -TileSize * 0.5f && y <= (Height * TileSize) - (TileSize * 0.5f));

        IntVector2 tilePos = GetPosAsTile(new Vector3(x, 0.0f, z));
        int regionX = tilePos.x / m_RegionWidth;
        int regionY = tilePos.y / m_RegionHeight;

        return(m_Regions[regionX][regionY].GetPositionHeight(tilePos, x, z));
    }



    public Vector2 GetTileCenterPosition(int x, int z) {
        return(GetTileCenterPosition(new IntVector2(x, z)));
    }

    public Vector2 GetTileCenterPosition(IntVector2 tileCoord) {
        Vector2 pos = new Vector2(tileCoord.x, tileCoord.y);
        pos *= m_TileSize;
        pos.x += m_TileSize * 0.5f;
        pos.y += m_TileSize * 0.5f;
        return(pos);
    }

	#endregion



    #region Terrain Editing

    public void ChangeHeightMap(int x, int y, float newHeight) {
        Assert.Test(x >= 0 && y >= 0 && x <= Width && y <= Height);
        m_HeightMap[GetHeightArrayIndex(x, y)] = newHeight;
    }



    public void ChangeTileHeight(int x, int y, float newHeight) {
        Assert.Test(x >= 0 && y >= 0 && x < Width && y < Height);
        ChangeHeightMap( x ,  y , newHeight);
        ChangeHeightMap(x+1,  y , newHeight);
        ChangeHeightMap( x , y+1, newHeight);
        ChangeHeightMap(x+1, y+1, newHeight);
    }



    public void RefreshTileMesh(int x, int y) {
        Assert.Test(x >= 0 && y >= 0 && x < Width && y < Height);

        int regionX = x / m_RegionWidth;
        int regionY = y / m_RegionHeight;
        UnityEngine.Profiling.Profiler.BeginSample("Level");
        m_Regions[regionX][regionY].RefreshTileMesh(x, y);
        UnityEngine.Profiling.Profiler.EndSample();
        UnityEngine.Profiling.Profiler.BeginSample("Refresh");
        m_Regions[regionX][regionY].RefreshMeshVerticesAndNormals();
        UnityEngine.Profiling.Profiler.EndSample();
    }

    #endregion
}
