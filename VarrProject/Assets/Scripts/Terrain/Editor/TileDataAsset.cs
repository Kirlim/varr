﻿using UnityEngine;
using UnityEditor;
using System.Collections;



public class TileDataAsset {
	[MenuItem("Assets/Create/Tile Data Asset")]
	public static void CreateAsset() {
		ScriptableObjectUtility.CreateAsset<TileData>();
	}
}
