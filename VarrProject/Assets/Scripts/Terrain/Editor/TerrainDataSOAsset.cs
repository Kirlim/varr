﻿using UnityEngine;
using UnityEditor;
using System.Collections;



public class TerrainDataSOAsset {
	[MenuItem("Assets/Create/Terrain Data SO Asset")]
	public static void CreateAsset() {
		ScriptableObjectUtility.CreateAsset<TerrainDataSO>();
	}
}