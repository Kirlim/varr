﻿using UnityEngine;
using UnityEditor;
using System.Collections;



public class TilesetDataAsset {
	[MenuItem("Assets/Create/Tileset Data Asset")]
	public static void CreateAsset() {
		ScriptableObjectUtility.CreateAsset<TilesetData>();
	}
}