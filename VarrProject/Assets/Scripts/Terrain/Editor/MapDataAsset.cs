﻿using UnityEngine;
using UnityEditor;
using System.Collections;



public class MapDataAsset {
    [MenuItem("Assets/Create/Map Data Asset")]
    public static void CreateAsset() {
        ScriptableObjectUtility.CreateAsset<MapData>();
    }
}
