﻿using UnityEngine;
using System.Collections;



/// <summary>
/// Represents a tile
/// </summary>
public class TileData : ScriptableObject {
	[Tooltip("Unique integer ID to this tile")]
	[SerializeField] protected int m_Id;
	[SerializeField] protected int m_GroupId;
	[SerializeField] protected TileTypes m_Type;
	[SerializeField] protected TileSubtypes m_Subtype;
	[SerializeField] protected Texture2D m_SourceTexture;

	public int Id { get { return(m_Id); } }
	public int GroupId { get { return(m_GroupId); } }
	public TileTypes TileType { get { return(m_Type); } }
	public TileSubtypes TileSubtype { get { return(m_Subtype); } }
	public Texture2D SourceTexture { get { return(m_SourceTexture); } }
}
