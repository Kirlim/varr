﻿/// <summary>
/// Orientations that a tile can have. East is positive x axis
/// </summary>
public enum TileOrientations {
	East = 0, North = 1, West = 2, South = 3
}