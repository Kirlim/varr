using UnityEngine;
using System.Collections;
using System.Collections.Generic;



/// <summary>
/// Represents a region of the map, for rendering purposes.
/// </summary>
public class MapRegion {
	private Map m_Owner;
	private Mesh m_Mesh;
	private int m_StartX, m_StartY;
	private int m_EndX, m_EndY;

	private Vector3[] m_Vertices;
	private Vector2[] m_Uv;
	private Vector2[] m_Uv1;
	private Vector3[] m_Normals;
	private Color[]   m_Colors;
	private int[]     m_Triangles;

    private bool m_FirstBuild;
    private OcTriangle[] m_OctreeTriangles;


	private enum TileShapeTypes { None, Plain, SimpleSlope, DiamondSlope }

	private MaterialPropertyBlock m_MatPropertyBlock;



	public MapRegion() {
		m_MatPropertyBlock = new MaterialPropertyBlock();
        m_FirstBuild = true;
	}



	public void Render() {
		if(m_Mesh != null) {
			Graphics.DrawMesh(
				m_Mesh,
				new Vector3(0, 0, 0),
				Quaternion.identity,
				m_Owner.Tileset.TilesetMaterial,
				LayerMask.NameToLayer("Default"), // TODO : GET RID OF MAGIC VALUE
				Camera.main,
				0,
				m_MatPropertyBlock,
				true,
				true
			);
		}
	}



	public void SetUp(Map map, int startX, int startY, int endX, int endY) {
		Assert.Test(startX >= 0 && startY >= 0);
		Assert.Test(endX < map.Width && endY < map.Height);
		Assert.Test(startX <= endX && startY <= endY);

		this.m_Owner = map;

		this.m_StartX = startX;
		this.m_StartY = startY;
		this.m_EndX = endX;
		this.m_EndY = endY;

		BuildRegionMesh();
	}



	public void BuildRegionMesh() {
		if(m_Mesh == null) {
			m_Mesh = BuildMesh();
		}
	}



	private int GetTileIndex(int x, int y) {
		return(
			((y - m_StartY) * (1 + m_EndX - m_StartX)) + (x - m_StartX)
		);
	}



	private bool HasTileOfType(int tileId) {
		for(int x = m_StartX; x <= m_EndX; ++x) {
			for(int y = m_StartY; y <= m_EndY; ++y) {
				if(m_Owner.GetTileId(x, y) == tileId) return(true);
			}
		}
		return(false);
	}


	
	private Mesh BuildMesh() {

		int tilesCount = (1 + m_EndX - m_StartX) * (1 + m_EndY - m_StartY);

		// Allocate lists
		Assert.Test(
			m_Vertices == null && m_Normals == null &&
			m_Uv == null && m_Uv1 == null &&
			m_Colors == null && m_Triangles == null
		);

		m_Vertices  = new Vector3[tilesCount * 6];
		m_Normals   = new Vector3[tilesCount * 6];
		m_Uv        = new Vector2[tilesCount * 6];
		m_Uv1       = new Vector2[tilesCount * 6];
		m_Colors    = new Color[tilesCount * 6];
		m_Triangles = new int[tilesCount * 6];

        m_OctreeTriangles = new OcTriangle[tilesCount * 2];


		// Feed with initial data, since lists sizes are constant
		for(int i = 0; i < tilesCount * 6; ++i) {
			m_Vertices[i]  = Vector3.zero;
			m_Normals[i]   = Vector3.zero;
			m_Uv[i]        = Vector3.zero;
			m_Uv1[i]       = Vector3.zero;
			m_Colors[i]    = Color.white;
			m_Triangles[i] = 0;
		}


		// Builds the mesh
		for(int x = m_StartX; x <= m_EndX; ++x) {
			for(int y = m_StartY; y <= m_EndY; ++y) {
                RefreshTileMesh(x, y);
			}
		}
        m_FirstBuild = false;


		// Set the mesh
		Mesh mesh = new Mesh();
		mesh.vertices  = m_Vertices;
		mesh.uv        = m_Uv;
		mesh.uv2       = m_Uv1;
		mesh.normals   = m_Normals;
		mesh.colors    = m_Colors;
		mesh.triangles = m_Triangles;
		;
		return(mesh);
	}



    public void RefreshTileMesh(int x, int y) {
        ValidateTileRange(x, y);

        // Values needed for processing stuff
        Vector2 uvStart = Vector2.zero;
        Vector2 uvEnd = Vector2.zero;
        Vector2 uvA = Vector2.zero, uvB = Vector2.zero, uvC = Vector2.zero, uvD = Vector2.zero;
        int tileId = -1;
        
        // Setup the tile mesh
        tileId = m_Owner.GetTileId(x, y);
        m_Owner.Tileset.GetTileUvs(tileId, out uvStart, out uvEnd);
        m_Owner.RotateUv(
            uvStart, uvEnd, m_Owner.GetTileOrientation(x, y), 
            out uvA, out uvB, out uvC, out uvD
        );
        
        TileShapeTypes tileType = GetTileShapeType(x, y);
        
        switch(tileType) {
        case TileShapeTypes.Plain:        SetupPlaneTile (x, y, uvA, uvB, uvC, uvD); break;
        case TileShapeTypes.SimpleSlope:  SetupPlaneTile (x, y, uvA, uvB, uvC, uvD); break;
        case TileShapeTypes.DiamondSlope: SetupFoldedTile(x, y, uvA, uvB, uvC, uvD); break;
        }
    }



    public void RefreshMeshVerticesAndNormals() {
        m_Mesh.vertices = m_Vertices;
        m_Mesh.normals = m_Normals;
    }



    public void RefreshUvs() {
        m_Mesh.uv = m_Uv;
        m_Mesh.uv2 = m_Uv1;
    }



	public void RefreshTilesUvs() {
		for(int x = m_StartX; x <= m_EndX; ++x) {
			for(int y = m_StartY; y <= m_EndY; ++y) {
				RefreshTileUv(x, y);
			}
		}
	}



	public void RefreshTileUv(int x, int y) {
		Vector2 uvStart = Vector2.zero;
		Vector2 uvEnd = Vector2.zero;
		Vector2 uvA = Vector2.zero, uvB = Vector2.zero, uvC = Vector2.zero, uvD = Vector2.zero;

		int tileId = m_Owner.GetTileId(x, y);
		m_Owner.Tileset.GetTileUvs(tileId, out uvStart, out uvEnd);
		m_Owner.RotateUv(
			uvStart, uvEnd, m_Owner.GetTileOrientation(x, y), 
			out uvA, out uvB, out uvC, out uvD
		);
		
		int baseVertex = GetTileIndex(x, y) * 6;
		
		TileShapeTypes tileType = GetTileShapeType(x, y);
		
		switch(tileType) {
		case TileShapeTypes.Plain:
			RefreshPlaneTileUvs (baseVertex, uvA, uvB, uvC, uvD); break;
		case TileShapeTypes.SimpleSlope:  
			RefreshPlaneTileUvs (baseVertex, uvA, uvB, uvC, uvD); break;
		case TileShapeTypes.DiamondSlope: 
			bool horCut = IsFoldAHorizontalCut(x, y);
			RefreshFoldedTileUvs(baseVertex, uvA, uvB, uvC, uvD, horCut); break;
		}
	}


	
	private TileShapeTypes GetTileShapeType(int x, int y) {
		// Stores heights for easier reading
		float hA = m_Owner.GetVertexHeight( x,   y );
		float hB = m_Owner.GetVertexHeight(x+1,  y );
		float hC = m_Owner.GetVertexHeight(x+1, y+1);
		float hD = m_Owner.GetVertexHeight( x,  y+1);

		TileShapeTypes tileShapeType = TileShapeTypes.Plain;

		if(!(hA == hB && hB == hC && hC == hD && hD == hA)) tileShapeType = TileShapeTypes.None;

		if(tileShapeType == TileShapeTypes.None) {
			if(
				FloatEquals(hA, hB) && FloatEquals(hC, hD) ||
				FloatEquals(hA, hD) && FloatEquals(hB, hC)
			) tileShapeType = TileShapeTypes.SimpleSlope;
		}

		if(tileShapeType == TileShapeTypes.None) {
			if(FloatEquals(hA, hC) || FloatEquals(hB, hD) ) tileShapeType = TileShapeTypes.DiamondSlope;
		}

		return(tileShapeType);
	}



	/// <summary>
	/// Calculates the normals of a tile
	/// </summary>
	/// <param name="v">v is the base vertex</param>
	private void CalculateNormals(int v) {
		m_Normals[ v ] = MyMath3D.GetNormalizedNormal(m_Vertices[v+0], m_Vertices[v+2], m_Vertices[v+1]);
		m_Normals[v+1] = MyMath3D.GetNormalizedNormal(m_Vertices[v+1], m_Vertices[v+0], m_Vertices[v+2]);
		m_Normals[v+2] = MyMath3D.GetNormalizedNormal(m_Vertices[v+2], m_Vertices[v+1], m_Vertices[v+0]);
		m_Normals[v+3] = MyMath3D.GetNormalizedNormal(m_Vertices[v+3], m_Vertices[v+5], m_Vertices[v+4]);
		m_Normals[v+4] = MyMath3D.GetNormalizedNormal(m_Vertices[v+4], m_Vertices[v+3], m_Vertices[v+5]);
		m_Normals[v+5] = MyMath3D.GetNormalizedNormal(m_Vertices[v+5], m_Vertices[v+4], m_Vertices[v+3]);
	}



	/// <summary>
	/// Calculates the triangles of a tile
	/// </summary>
	/// <param name="v">v is the base vertex</param>
	private void CalculateTriangles(int v) {
		// TODO : COULD BE PROCESSED ONLY ONCE AT BUILD MESH
		m_Triangles[ v ] = v+5;
		m_Triangles[v+1] = v+4;
		m_Triangles[v+2] = v+3;
		m_Triangles[v+3] = v+2;
		m_Triangles[v+4] = v+1;
		m_Triangles[v+5] = v+0;
	}







	private void SetupPlaneTile(int x, int y, Vector2 uvA, Vector2 uvB, Vector2 uvC, Vector2 uvD) {
		// Tile vertices positioning
		//     2       5---4
		//   / |       | /
		// 0---1       3

		float tx = x * m_Owner.TileSize;    // tile x
		float ty = y * m_Owner.TileSize;    // tile y
        float ts = m_Owner.TileSize;        // tile size

		int v = GetTileIndex(x, y) * 6; // vertex "zero"

		m_Vertices[ v ] = new Vector3(tx     , m_Owner.GetVertexHeight( x ,  y ), ty     );
		m_Vertices[v+1] = new Vector3(tx + ts, m_Owner.GetVertexHeight(x+1,  y ), ty     );
		m_Vertices[v+2] = new Vector3(tx + ts, m_Owner.GetVertexHeight(x+1, y+1), ty + ts);
		m_Vertices[v+3] = new Vector3(tx     , m_Owner.GetVertexHeight( x ,  y ), ty     );
		m_Vertices[v+4] = new Vector3(tx + ts, m_Owner.GetVertexHeight(x+1, y+1), ty + ts);
		m_Vertices[v+5] = new Vector3(tx     , m_Owner.GetVertexHeight( x , y+1), ty + ts);

		m_Colors[ v ] = GetVertexColor(x+0, y+0);
		m_Colors[v+1] = GetVertexColor(x+1, y+0);
		m_Colors[v+2] = GetVertexColor(x+1, y+1);
		m_Colors[v+3] = GetVertexColor(x+0, y+0);
		m_Colors[v+4] = GetVertexColor(x+1, y+1);
		m_Colors[v+5] = GetVertexColor(x+0, y+1);

		RefreshPlaneTileUvs(v, uvA, uvB, uvC, uvD);
		CalculateNormals(v);
		CalculateTriangles(v);


        UnityEngine.Profiling.Profiler.BeginSample("Octree");
        int ocIndex = GetTileIndex(x, y) * 2;
        if(!m_FirstBuild) {
            m_Owner.terrainOctree.Remove(m_OctreeTriangles[ocIndex]);
            m_Owner.terrainOctree.Remove(m_OctreeTriangles[ocIndex+1]);
        }

        m_OctreeTriangles[ocIndex]   = new OcTriangle(m_Vertices[v+0], m_Vertices[v+1], m_Vertices[v+2]);
        m_OctreeTriangles[ocIndex+1] = new OcTriangle(m_Vertices[v+3], m_Vertices[v+4], m_Vertices[v+5]);

		m_Owner.terrainOctree.Insert(m_OctreeTriangles[ocIndex]);
		m_Owner.terrainOctree.Insert(m_OctreeTriangles[ocIndex+1]);
        UnityEngine.Profiling.Profiler.EndSample();
	}



	private void SetupFoldedTile(int x, int y, Vector2 uvA, Vector2 uvB, Vector2 uvC, Vector2 uvD) {
		// Tile vertices positioning (vertical cut)
		//     2       5---4
		//   / |       | /
		// 0---1       3

		// Tile vertices positioning (horizontal cut)
		// 2           5---4
		// | \           \ |
		// 0---1           3
		
		float tx = x * m_Owner.TileSize;    // tile x
		float ty = y * m_Owner.TileSize;    // tile y
		float ts = m_Owner.TileSize;        // tile size
		
		int v = GetTileIndex(x, y) * 6; // vertex "zero"
		
		bool horizontalCut = IsFoldAHorizontalCut(x, y);
		
		if(horizontalCut) {
            m_Vertices[ v ] = new Vector3(tx     , m_Owner.GetVertexHeight( x ,  y ), ty     );
            m_Vertices[v+1] = new Vector3(tx + ts, m_Owner.GetVertexHeight(x+1,  y ), ty     );
            m_Vertices[v+2] = new Vector3(tx     , m_Owner.GetVertexHeight( x , y+1), ty + ts);
            m_Vertices[v+3] = new Vector3(tx + ts, m_Owner.GetVertexHeight(x+1,  y ), ty     );
            m_Vertices[v+4] = new Vector3(tx + ts, m_Owner.GetVertexHeight(x+1, y+1), ty + ts);
            m_Vertices[v+5] = new Vector3(tx     , m_Owner.GetVertexHeight( x , y+1), ty + ts);
			
            m_Colors[ v ] = GetVertexColor(x+0, y+0);
            m_Colors[v+1] = GetVertexColor(x+1, y+0);
            m_Colors[v+2] = GetVertexColor(x+0, y+1);
            m_Colors[v+3] = GetVertexColor(x+1, y+0);
            m_Colors[v+4] = GetVertexColor(x+1, y+1);
            m_Colors[v+5] = GetVertexColor(x+0, y+1);
		} else {
            m_Vertices[ v ] = new Vector3(tx     , m_Owner.GetVertexHeight( x ,  y ), ty     );
            m_Vertices[v+1] = new Vector3(tx + ts, m_Owner.GetVertexHeight(x+1,  y ), ty     );
            m_Vertices[v+2] = new Vector3(tx + ts, m_Owner.GetVertexHeight(x+1, y+1), ty + ts);
            m_Vertices[v+3] = new Vector3(tx     , m_Owner.GetVertexHeight( x ,  y ), ty     );
            m_Vertices[v+4] = new Vector3(tx + ts, m_Owner.GetVertexHeight(x+1, y+1), ty + ts);
            m_Vertices[v+5] = new Vector3(tx     , m_Owner.GetVertexHeight( x , y+1), ty + ts);
			

            m_Colors[ v ] = GetVertexColor(x+0, y+0);
            m_Colors[v+1] = GetVertexColor(x+1, y+0);
            m_Colors[v+2] = GetVertexColor(x+1, y+1);
            m_Colors[v+3] = GetVertexColor(x+0, y+0);
            m_Colors[v+4] = GetVertexColor(x+1, y+1);
            m_Colors[v+5] = GetVertexColor(x+0, y+1);
		}

		RefreshFoldedTileUvs(v, uvA, uvB, uvC, uvD, horizontalCut);
		CalculateNormals(v);
		CalculateTriangles(v);


        UnityEngine.Profiling.Profiler.BeginSample("Octree");
        int ocIndex = GetTileIndex(x, y) * 2;
        if(!m_FirstBuild) {
            m_Owner.terrainOctree.Remove(m_OctreeTriangles[ocIndex]);
            m_Owner.terrainOctree.Remove(m_OctreeTriangles[ocIndex+1]);
        }
        
        m_OctreeTriangles[ocIndex]   = new OcTriangle(m_Vertices[v+0], m_Vertices[v+1], m_Vertices[v+2]);
        m_OctreeTriangles[ocIndex+1] = new OcTriangle(m_Vertices[v+3], m_Vertices[v+4], m_Vertices[v+5]);
        
        m_Owner.terrainOctree.Insert(m_OctreeTriangles[ocIndex]);
        m_Owner.terrainOctree.Insert(m_OctreeTriangles[ocIndex+1]);
        UnityEngine.Profiling.Profiler.EndSample();
	}

	private bool IsFoldAHorizontalCut(int x, int y) {
		return(FloatEquals(m_Owner.GetVertexHeight(x+1, y), m_Owner.GetVertexHeight(x, y+1)));
	}



	private void RefreshPlaneTileUvs(
		int baseVertex, Vector2 uvA, Vector2 uvB, Vector2 uvC, Vector2 uvD
	) {
		m_Uv[ baseVertex ] = new Vector2(uvA.x, uvA.y);
		m_Uv[baseVertex+1] = new Vector2(uvB.x, uvB.y);
		m_Uv[baseVertex+2] = new Vector2(uvC.x, uvC.y);
		m_Uv[baseVertex+3] = new Vector2(uvA.x, uvA.y);
		m_Uv[baseVertex+4] = new Vector2(uvC.x, uvC.y);
		m_Uv[baseVertex+5] = new Vector2(uvD.x, uvD.y);
		
		m_Uv1[ baseVertex ] = m_Uv[ baseVertex ];
		m_Uv1[baseVertex+1] = m_Uv[baseVertex+1];
		m_Uv1[baseVertex+2] = m_Uv[baseVertex+2];
		m_Uv1[baseVertex+3] = m_Uv[baseVertex+3];
		m_Uv1[baseVertex+4] = m_Uv[baseVertex+4];
		m_Uv1[baseVertex+5] = m_Uv[baseVertex+5];
	}



	private void RefreshFoldedTileUvs(
		int baseVertex, Vector2 uvA, Vector2 uvB, Vector2 uvC, Vector2 uvD, bool horCut
	) {
		if(horCut) {
			m_Uv[ baseVertex ] = new Vector2(uvA.x, uvA.y);
			m_Uv[baseVertex+1] = new Vector2(uvB.x, uvB.y);
			m_Uv[baseVertex+2] = new Vector2(uvC.x, uvC.y);
			m_Uv[baseVertex+3] = new Vector2(uvA.x, uvA.y);
			m_Uv[baseVertex+4] = new Vector2(uvC.x, uvC.y);
			m_Uv[baseVertex+5] = new Vector2(uvD.x, uvD.y);
			
			m_Uv1[ baseVertex ] = m_Uv[ baseVertex ];
			m_Uv1[baseVertex+1] = m_Uv[baseVertex+1];
			m_Uv1[baseVertex+2] = m_Uv[baseVertex+2];
			m_Uv1[baseVertex+3] = m_Uv[baseVertex+3];
			m_Uv1[baseVertex+4] = m_Uv[baseVertex+4];
			m_Uv1[baseVertex+5] = m_Uv[baseVertex+5];
		} else {
			m_Uv[ baseVertex ] = new Vector2(uvA.x, uvA.y);
			m_Uv[baseVertex+1] = new Vector2(uvB.x, uvB.y);
			m_Uv[baseVertex+2] = new Vector2(uvD.x, uvD.y);
			m_Uv[baseVertex+3] = new Vector2(uvB.x, uvB.y);
			m_Uv[baseVertex+4] = new Vector2(uvC.x, uvC.y);
			m_Uv[baseVertex+5] = new Vector2(uvD.x, uvD.y);
			
			m_Uv1[ baseVertex ] = m_Uv[ baseVertex ];
			m_Uv1[baseVertex+1] = m_Uv[baseVertex+1];
			m_Uv1[baseVertex+2] = m_Uv[baseVertex+2];
			m_Uv1[baseVertex+3] = m_Uv[baseVertex+3];
			m_Uv1[baseVertex+4] = m_Uv[baseVertex+4];
			m_Uv1[baseVertex+5] = m_Uv[baseVertex+5];
		}
	}


    // TODO : put this constant in a better place.
    public Vector3 East = new Vector3(1.0f, 0.0f, 0.0f);
	private Color GetVertexColor(int x, int y) {
		float lightStart = 0.0f;
		float lightEnd = 1.0f;

		float height = m_Owner.GetVertexHeight(x, y);

		float startAtan = 0.0f;
		for(int i = m_Owner.Width; i > x; --i) {
			float horDist = (i - x) * m_Owner.TileSize;
			float verDist = m_Owner.GetVertexHeight(i, y) - height;
			if(verDist < 0.0f) continue;
			float atan = Mathf.Atan2(verDist, horDist) / Mathf.PI;
			if(atan > startAtan) {
				startAtan = atan;
				lightStart = atan;
			}
		}

		float endAtan = 1.0f;
		for(int i = 0; i < x; ++i) {
			float horDist = (i - x) * m_Owner.TileSize;
			float verDist = m_Owner.GetVertexHeight(i, y) - height;
			float atan = Mathf.Atan2(verDist, horDist) / Mathf.PI;
			if(verDist < 0.0f) continue;
			if(atan < endAtan) {
				endAtan = atan;
				lightEnd = atan;
			}
		}

		return(new Color(lightStart, lightEnd, 1.0f, 1.0f));
	}



    /// <summary>
    /// Returns the height of a terrain position, based on its underlining
    /// triangles.
    /// </summary>
    /// <returns>The position height.</returns>
    /// <param name="x">The x coordinate.</param>
    /// <param name="z">The z coordinate.</param>
    public float GetPositionHeight(IntVector2 tilePos, float x, float z) {
        ValidateTileRange(tilePos);
        int v = GetTileIndex(tilePos.x, tilePos.y) * 6; // Base vertex index

        // TODO : GET RID OF MAGIC NUMBERS

        float height = -1.0f;
        Ray ray = new Ray(new Vector3(x, 0.0f, z), Vector3.up);
        if(MyMath3D.RayTriIntersectTrumbore(
            ray, m_Vertices[v], m_Vertices[v+1], m_Vertices[v+2], 0.001f, out height
        )) {
            return(height);
        } else if(MyMath3D.RayTriIntersectTrumbore(
            ray, m_Vertices[v+3], m_Vertices[v+4], m_Vertices[v+5], 0.001f, out height
        )) {
            return(height);
        }

        Debug.Log("Failed to get position height: " + x + " " + z);
        return(height);
    }



    public bool CanHoldBuildings(int tileX, int tileY) {
        ValidateTileRange(tileX, tileY);

        TileShapeTypes tileShape = GetTileShapeType(tileX, tileY);
        return(tileShape == TileShapeTypes.Plain);
    }



    public bool CanHoldRoads(int tileX, int tileY) {
        ValidateTileRange(tileX, tileY);

        TileShapeTypes tileShape = GetTileShapeType(tileX, tileY);
        return(tileShape == TileShapeTypes.Plain || tileShape == TileShapeTypes.SimpleSlope);
    }



	private const float m_Delta = 0.01f;
	private bool FloatEquals(float a, float b) {
		return(a < b + m_Delta && a > b - m_Delta);
	}



    [System.Diagnostics.Conditional("DEBUG_GAME")]
    protected void ValidateTileRange(int x, int y) {
        ValidateTileRange(new IntVector2(x, y));
    }

    [System.Diagnostics.Conditional("DEBUG_GAME")]
    protected void ValidateTileRange(IntVector2 tilePos) {
        Assert.Test(tilePos.x >= m_StartX && tilePos.y >= m_StartY);
        Assert.Test(tilePos.x <= m_EndX && tilePos.y <= m_EndY);
    }
}
