﻿using UnityEngine;
using System.Collections;



/// <summary>
/// Stores the information of a tile in the map for use during runtime.
/// </summary>
public struct MapTile {
	public TileData tile;
	public TileOrientations tileOrientation;
	public MapObject mapObject;

	public bool IsFree { get { return(mapObject == null); } }

	// TODO : ADD NEEDED FLAGS
}
