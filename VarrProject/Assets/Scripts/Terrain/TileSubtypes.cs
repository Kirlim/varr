﻿/// <summary>
/// Tile subtypes. Indicates the intended shape of the tile
/// </summary>
public enum TileSubtypes {
	Single = 0,
	SingleEnd = 1, Straight = 2, Turn = 3, Intersection = 4, Cross = 5
}
