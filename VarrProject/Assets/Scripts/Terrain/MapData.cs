﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



/// <summary>
/// Holds information that composes a map, such as terrain information,
/// map objects placed, etc
/// </summary>
public class MapData : ScriptableObject {
    public TerrainDataSO terrainData;
    public List<MapObjectPlacementData> placedObjects;
}
