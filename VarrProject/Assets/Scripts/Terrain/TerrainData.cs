﻿using UnityEngine;
using System.Collections;



[System.Serializable]
public class TerrainData {
	[SerializeField] [HideInInspector]
	public int width;

	[SerializeField] [HideInInspector]
	public int height;

	[SerializeField] [HideInInspector]
	public float[] heightMap;

	[SerializeField] [HideInInspector]
	public int[] tilesIds;

	[SerializeField] [HideInInspector]
	public TileOrientations[] tilesOrientations;



    public int Width { get { return(width); } }
    public int Height { get { return(height); } }

    public bool Valid {
        get { 
            int size = width * height;
            int heightSize = (width+1) * (height+1);

            return(
                width > 0 && height > 0 &&
                heightMap != null && tilesIds != null && tilesOrientations != null &&
                heightMap.Length == heightSize && tilesIds.Length == size &&
                tilesOrientations.Length == size
            );
        }
    }



    public void CreateEmpty(int width, int height, float defaultHeight, int defaultTileId) {
        this.width = width;
        this.height = height;

        heightMap = new float[(width+1) * (height+1)];
        tilesIds = new int[width * height];
        tilesOrientations = new TileOrientations[width * height];

        for(int x = 0; x <= width; ++x) {
            for(int y = 0; y <= height; ++y) {
                heightMap[y * (height+1) + x] = defaultHeight;
            }
        }

        for(int x = 0; x < width; ++x) {
            for(int y = 0; y < height; ++y) {
                int tile = y * height + x;
                tilesIds[tile] = defaultTileId;
                tilesOrientations[tile] = TileOrientations.East;
            }
        }
    }



    public void ChangeHeightMap(int x, int y, float newHeight) {
        Assert.Test(x >= 0 && y >= 0 && x <= width && y <= height);
        heightMap[y * (width + 1) + x] = newHeight;
    }



    public void ChangeTileHeight(int x, int y, float newHeight) {
        Assert.Test(x >= 0 && y >= 0 && x < width && y < height);
        ChangeHeightMap( x ,  y , newHeight);
        ChangeHeightMap(x+1,  y , newHeight);
        ChangeHeightMap( x , y+1, newHeight);
        ChangeHeightMap(x+1, y+1, newHeight);
    }
}
