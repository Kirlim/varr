﻿using UnityEngine;
using System.Collections;



public class MapDebugDrawBridge : MonoBehaviour {
    public Map mapRef;

    protected void OnPostRender() {
        mapRef.DebugDraw();
    }
}
