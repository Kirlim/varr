﻿using UnityEngine;
using System.Collections;

public class TilesetTextureCreator : MonoBehaviour {
	public TilesetData tilesetData;

	[ContextMenu("Generate Texture")]
	public void GenerateTilesetTexture() {
		tilesetData.GenerateTilesetTexture();
	}
}
