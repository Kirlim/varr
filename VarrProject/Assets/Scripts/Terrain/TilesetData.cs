﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif



public class TilesetData : ScriptableObject {
	[SerializeField] protected int m_TextureDim;
	[SerializeField] protected TileData[] m_TilesData;
	[SerializeField] protected Rect[] m_Uvs;
	[SerializeField] protected Material m_TilesetMaterial;

	public Material TilesetMaterial { get { return(m_TilesetMaterial); } }
	public int Length { get { return(m_TilesData.Length); } }



	public TileData GetTile(int id) {
		// TODO : OPTIMIZE THIS CALL
		TileData result = null;
		for(int i = 0; i < m_TilesData.Length; ++i) {
			if(m_TilesData[i].Id == id) {
				result = m_TilesData[i];
				break;
			}
		}

		return(result);
	}



	public TileData GetTile(int groupId, TileTypes tileType, TileSubtypes tileSubtype) {
		TileData tileData = null;

		for(int i = 0; i < m_TilesData.Length; ++i) {
			TileData c = m_TilesData[i];
			if(c.GroupId == groupId && c.TileType == tileType && c.TileSubtype == tileSubtype) {
				tileData = c;
				break;
			}
		}

		return(tileData);
	}



	public int GetTileGroup(int id) {
		return(m_TilesData[id].GroupId);
	}



	public TileTypes GetTileType(int id) {
		return(m_TilesData[id].TileType);
	}



	public TileSubtypes GetTileSubtype(int id) {
		return(m_TilesData[id].TileSubtype);
	}



	public void GetTileUvs(int tileId, out Vector2 start, out Vector2 end) {
//		start = m_Uvs[tileId].min;
//		end = m_Uvs[tileId].max;
		Vector2 fix = m_TilesetMaterial.mainTexture.texelSize;
		start = m_Uvs[tileId].position + fix;
		end = start + m_Uvs[tileId].size - fix;
	}



	[ContextMenu("Generate Texture")]
	public void GenerateTilesetTexture() {
#if UNITY_EDITOR
		// TODO : +1 IS NOT NEEDED FOR PERFECT SQUARES
		int sideTilesCount = (int) (Mathf.Sqrt(m_TilesData.Length)) + 1;

		int newTextDim = m_TextureDim * sideTilesCount;
		if(newTextDim > 2    && newTextDim < 4)    newTextDim = 4;
		if(newTextDim > 4    && newTextDim < 8)    newTextDim = 8;
		if(newTextDim > 8    && newTextDim < 16)   newTextDim = 16;
		if(newTextDim > 16   && newTextDim < 32)   newTextDim = 32;
		if(newTextDim > 32   && newTextDim < 64)   newTextDim = 64;
		if(newTextDim > 64   && newTextDim < 128)  newTextDim = 128;
		if(newTextDim > 128  && newTextDim < 256)  newTextDim = 256;
		if(newTextDim > 256  && newTextDim < 512)  newTextDim = 512;
		if(newTextDim > 512  && newTextDim < 1024) newTextDim = 1024;
		if(newTextDim > 1024 && newTextDim < 2048) newTextDim = 2048;

		Texture2D[] textures = new Texture2D[m_TilesData.Length];
		for(int i = 0; i < textures.Length; ++i) textures[i] = m_TilesData[i].SourceTexture;

		Texture2D newTexture = new Texture2D(newTextDim, newTextDim);
		Rect[] uvs = newTexture.PackTextures(textures, 0);

		int maxTileId = 0;
		for(int i = 0; i < m_TilesData.Length; ++i) {
			if(maxTileId < m_TilesData[i].Id) maxTileId = m_TilesData[i].Id;
		}
		m_Uvs = new Rect[maxTileId+1];

		for(int i = 0; i < uvs.Length; ++i) {
			int tileId = m_TilesData[i].Id;
			m_Uvs[tileId] = uvs[i];
		}
		AssetDatabase.CreateAsset(newTexture, "Assets/GameData/MapAssets/TilesetTexture");
		if(AssetDatabase.Contains(newTexture)) Debug.Log("Texture asset created");

		m_TilesetMaterial.mainTexture = newTexture;
#endif
	}
}
