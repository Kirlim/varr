﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif



public class MapEditor : MonoBehaviour {
    public MapData mapData;
    public int width, height;
    public int defaultHeight;
    public TileData defaultTile;

    public Map mapRef;


    public TerrainDataSO DestTerrainData { get { return(mapData.terrainData); } }


    // Now during editing
    protected MapEditorActions m_Action;
    protected int m_HeightLevel;
    protected string m_PlaceObjectId;


    protected void Awake() {
        Assert.Test(mapData != null);
        Assert.Test(mapRef != null);
    }



    [ContextMenu("Re-Generate if Needed")]
    public void RegenerateIfNeeded() {
        if(DestTerrainData == null) {
            Assert.Throw();
            return;
        }

        TerrainData data = DestTerrainData.data;
        if(!data.Valid || data.Width != width || data.Height != height) {
            ForceRegenerate();
        }
    }



    [ContextMenu("Force Re-Generate")]
    public void ForceRegenerate() {
        TerrainData data = DestTerrainData.data;
        data.CreateEmpty(width, height, defaultHeight * mapRef.TileSize, defaultTile.Id);
#if UNITY_EDITOR
        UnityEditor.EditorUtility.SetDirty(DestTerrainData);
#endif
    }



    [ContextMenu("Validate")]
    public void Validate() {
        if(DestTerrainData == null || !DestTerrainData.data.Valid) {
            Debug.Log("Map is invalid");
        } else {
            Debug.Log("Map is valid");
        }
    }



    protected void Update() {
        bool hasTile = false;
        int tx = -1, ty = -1;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        OctreeRaycastHit hit; // TODO : MAYBE OCTREE RAYCAST HIT SHOULD BE A STRUCT
        
        if(mapRef.terrainOctree.Raycast(ray, float.MaxValue, 0x0, out hit)) {
            Vector3 pos = hit.Content.WorldBounds.center;
            IntVector2 tileCoord = Global.Map.GetPosAsTile(pos);
            tx = tileCoord.x;
            ty = tileCoord.y;
            hasTile = true;
        } else {
            return;
        }

        bool leftClick = false;
        if(
            Input.GetMouseButton(0) &&
            !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()
        ) {
            leftClick = true;
        }



        switch(m_Action) {
        case MapEditorActions.LevelTerrain: {
            if(hasTile && leftClick) {
                if(tx >= 0 && ty >= 0 && tx < mapRef.Width && ty < mapRef.Height) {
                    DestTerrainData.data.ChangeTileHeight(tx, ty, m_HeightLevel * mapRef.TileSize);
                    mapRef.ChangeTileHeight(tx, ty, m_HeightLevel * mapRef.TileSize);

                    for(int x = Mathf.Max(0, tx-1); x <= Mathf.Min(mapRef.Width-1, tx+1); ++x) {
                        for(int y = Mathf.Max(0, ty-1); y <= Mathf.Min(mapRef.Height-1, ty+1); ++y) {
                            mapRef.RefreshTileMesh(x, y);
                        }
                    }
                }
            }
        } break;
        

        case MapEditorActions.PlaceObject: {
            if(leftClick) {
                if(Global.MapObjectsManager.Place(
                    m_PlaceObjectId, new IntVector2(tx, ty), Orientations.East
                )) {
                    mapData.placedObjects.Add(new MapObjectPlacementData(
                        m_PlaceObjectId, new IntVector2(tx, ty), Orientations.East
                    ));
                }
            }
        } break;
        }



#if UNITY_EDITOR
        if(Input.GetKeyDown(KeyCode.O)) {
            EditorUtility.SetDirty(mapData);
        }
        if(Input.GetKeyDown(KeyCode.P)) {
            EditorUtility.SetDirty(DestTerrainData);
        }
#endif
    }



    public void SetLevelTerrainAction() {
        m_Action = MapEditorActions.LevelTerrain;
    }



    public void IncreaseTerrainLevel() {
        m_HeightLevel += 1;
    }

    public void DecreaseTerrainLevel() {
        m_HeightLevel -= 1;
        m_HeightLevel = Mathf.Max(m_HeightLevel, 0);
    }



    public void SetPlaceSmallTree() {
        m_Action = MapEditorActions.PlaceObject;
        m_PlaceObjectId = MOIds.SmallTree;
    }



    public void SetPlaceMediumTree() {
        m_Action = MapEditorActions.PlaceObject;
        m_PlaceObjectId = MOIds.MediumTree;
    }



    public void SetPlaceLargeTree() {
        m_Action = MapEditorActions.PlaceObject;
        m_PlaceObjectId = MOIds.LargeTree;
    }
}
