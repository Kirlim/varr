﻿using UnityEngine;
using System.Collections;



public class MapObjectFarm : MapObject {
    public FarmBehavior behaviorRef;



    protected override void CloneAwake() {
        Global.FarmsManager.RegisterFarm(behaviorRef as IFarm);
    }
}
