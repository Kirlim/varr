﻿using UnityEngine;
using System.Collections;



public class MapObject : MonoBehaviour {
    [SerializeField] [Immutable] protected MapObjectData objectData;

    public IntVector2 Size { get { return(objectData.size); } }

	protected IntVector2 m_PosMin;
	protected IntVector2 m_PosMax;
	protected Orientations m_Orientation;

	public IntVector2 PosMin { get { return(m_PosMin); } }
	public IntVector2 PosMax { get { return(m_PosMax); } }
	public Orientations Orientation { get { return(m_Orientation); } }

	protected Transform m_Transform;



	protected void Awake() {
		m_Transform = this.transform;
	}



	public void SetPosition(Map map, int positionX, int positionZ) {
		// Calculate the z position
		m_PosMin = new IntVector2(
			Mathf.Max(positionX, 0),
			Mathf.Max(positionZ, 0)
		);
		m_PosMax = new IntVector2(
			Mathf.Min(Global.Map.Width  - 1, positionX + Size.x - 1),
			Mathf.Min(Global.Map.Height - 1, positionZ + Size.y - 1)
		);

        Vector2 position = Global.Map.GetTileCenterPosition(positionX, positionZ);

		float height = 0.0f;
		for(int x = m_PosMin.x; x <= m_PosMax.x; ++x) {
			for(int y = m_PosMin.y; y <= m_PosMax.y; ++y) {
                height = Mathf.Max(height, map.GetTileHeight(x, y));
			}
		}

		// Position the object
		m_Transform.position = new Vector3(position.x, height, position.y);
	}



    /// <summary>
    /// Used to "clone" thi map object. It will simply call Instantiate in
    /// this game object. Then, will call CloneAwake method on the new object,
    /// and return the clone reference
    /// </summary>
    public MapObject CloneMapObject() {
        GameObject newGo = Instantiate(this.gameObject) as GameObject;

        MapObject mapObject = newGo.GetComponent<MapObject>();
        mapObject.CloneAwake();

        return(mapObject);
    }



    protected virtual void CloneAwake() { }
}
