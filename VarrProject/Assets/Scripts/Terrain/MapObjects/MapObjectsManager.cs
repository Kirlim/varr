﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



/// <summary>
/// Keeps stored all map objects by id in a dictionary, for fast
/// access. Also has methods for spawning new map objects, by id.
/// </summary>
public class MapObjectsManager : MonoBehaviour {
    [Immutable] public MapObjectData[] objectsFeed;
    protected Dictionary<string, MapObjectData> m_MapObjectsDatas;



    protected void Awake() {
        m_MapObjectsDatas = new Dictionary<string, MapObjectData>();
        for(int i = 0; i < objectsFeed.Length; ++i) {
            Assert.Test(objectsFeed[i].prefab != null);
            m_MapObjectsDatas.Add(objectsFeed[i].id, objectsFeed[i]);
        }
    }



    public MapObjectData GetData(string id) {
        Assert.Test(m_MapObjectsDatas.ContainsKey(id));
        return(m_MapObjectsDatas[id]);
    }



    public MapObject GetPrefab(string id) {
        Assert.Test(m_MapObjectsDatas.ContainsKey(id));
        Assert.Test(m_MapObjectsDatas[id].prefab != null);
        return(m_MapObjectsDatas[id].prefab);
    }



    public bool CanPlace(string id, IntVector2 position, Orientations orientation) {
        MapObjectData objectData = GetData(id);

        IntVector2 min = position;
        IntVector2 max = position + objectData.size - new IntVector2(1, 1);

        return(Global.Map.CanPlaceBuilding(min, max));
    }



    public bool Place(string id, IntVector2 position, Orientations orientation) {
        if(!CanPlace(id, position, orientation)) return(false);

        MapObject newMapObject = GetPrefab(id).CloneMapObject();
        newMapObject.SetPosition(Global.Map, position.x, position.y);

        return(Global.Map.BuildStructure(newMapObject));
    }
}
