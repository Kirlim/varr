﻿using UnityEngine;
using UnityEditor;
using System.Collections;



public class MapObjectDataAsset : MonoBehaviour {
    [MenuItem("Assets/Create/Map Object Data Asset")]
    public static void CreateAsset() {
        ScriptableObjectUtility.CreateAsset<MapObjectData>();
    }
}
