﻿using UnityEngine;
using System.Collections;



/// <summary>
/// Stores data for a type of map object, and a reference to its prefab
/// </summary>
public class MapObjectData : ScriptableObject {
    public string id;
    public MapObject prefab;

    public IntVector2 size;
}
