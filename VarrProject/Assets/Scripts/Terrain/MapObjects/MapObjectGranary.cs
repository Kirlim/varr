﻿using UnityEngine;
using System.Collections;



public class MapObjectGranary : MapObject {
    public GranaryBehavior behaviorRef;



    protected override void CloneAwake() {
        Global.GranariesManager.RegisterGranary(behaviorRef as IGranary);
    }
}
