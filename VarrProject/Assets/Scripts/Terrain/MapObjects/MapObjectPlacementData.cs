﻿using UnityEngine;
using System.Collections;



/// <summary>
/// Simple storage class used to store information about placed
/// map objects.
/// </summary>
[System.Serializable]
public class MapObjectPlacementData {
    public string id;
    public IntVector2 position;
    public Orientations orientation;



    public MapObjectPlacementData(string id, IntVector2 position, Orientations orientation) {
        this.id = id;
        this.position = position;
        this.orientation = orientation;
    }
}
