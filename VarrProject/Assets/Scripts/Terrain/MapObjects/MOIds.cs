﻿using UnityEngine;
using System.Collections;



/// <summary>
/// Contains constants expected for the most common map objects. To avoid
/// magic values
/// </summary>
public class MOIds : MonoBehaviour {
    public const string WheatFarm = "WheatFarm";
    public const string Granary = "Granary";

    public const string FarmersHouse = "FarmersHouse";
    public const string GuardsHouse = "GuardsHouse";

    public const string SmallTree = "SmallTree";
    public const string MediumTree = "MediumTree";
    public const string LargeTree = "LargeTree";
}
