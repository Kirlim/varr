using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class Octree : OctreeBase {
	private OctreeNode m_RootNode;
	private int m_MaxDepth;

    protected OctreeNodesPool m_NodesPool;



	public Octree(Bounds bounds, int maxDepth) {
        m_NodesPool = new OctreeNodesPool();

		m_MaxDepth = maxDepth;
		m_RootNode = SpawnNode().Init(this, bounds, 0, m_MaxDepth);
	}



    public override void Init(Bounds bounds, int maxDepth) {
        throw new System.NotImplementedException();
    }



    public override int GetMaxDepth() {
        return(m_MaxDepth);
    }


    public void AddToPool(OctreeNode node) {
        m_NodesPool.AddToPool(node);
    }

    public OctreeNode SpawnNode() {
        return(m_NodesPool.GetNewNode());
    }



	public override IOctreeContent Insert(IOctreeContent content) {
		m_RootNode.Add(content);
        return(content);
	}



    public override IOctreeContent Remove(IOctreeContent content) {
        m_RootNode.Remove(content);
        return(content);
    }



    public override bool Raycast(Ray ray, float distance, int mask, out OctreeRaycastHit hit) {
		ray.direction = ray.direction.normalized;
		return(m_RootNode.Raycast(ray, distance, out hit, mask));
	}



	public override void DebugDraw() {
		m_RootNode.OnPostRender();
	}
}
