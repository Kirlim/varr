﻿using UnityEngine;
using System.Collections;



public struct OctreeRaycastHit {
	public float Distance { get; set; }
	public IOctreeContent Content { get; set; }
	public Vector3 Position { get; set; }



	public OctreeRaycastHit(float distance, Vector3 position, IOctreeContent content) {
		this.Distance = distance;
		this.Position = position;
		this.Content = content;
	}
}
