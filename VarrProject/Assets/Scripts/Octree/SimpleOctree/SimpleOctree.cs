﻿using UnityEngine;
using System.Collections;



/// <summary>
/// The simple octree will simply allocate all the the childs
/// and push Contents to the leaves. It will waste memory!
/// </summary>
public class SimpleOctree : OctreeBase {
    protected SimpleOctreeNode m_Root;
    protected int m_MaxDepth;



    public override void Init(Bounds bounds, int maxDepth) {
        this.m_MaxDepth = maxDepth;
        m_Root = new SimpleOctreeNode(this, bounds, 0);
    }



    public override IOctreeContent Insert(IOctreeContent content) {
        bool success = m_Root.Insert(content);
        Assert.Test(success);
        return(content);
    }



    public override IOctreeContent Remove(IOctreeContent content) {
        bool success = m_Root.Remove(content);
        Assert.Test(success);
        return(content);
    }



    public override bool Raycast(Ray ray, float distance, int mask, out OctreeRaycastHit hit) {
        ray.direction = ray.direction.normalized;
        return(m_Root.Raycast(ray, distance, out hit, mask));
    }



    public override int GetMaxDepth() {
        return(m_MaxDepth);
    }



    public override void DebugDraw() {
        m_Root.OnPostRender();
    }
}
