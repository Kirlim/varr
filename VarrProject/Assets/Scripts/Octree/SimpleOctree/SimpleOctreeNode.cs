using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class SimpleOctreeNode {
    // Children order:
    // - Bottom Layer
    // - Top Layer
    // And in the layers
    // - Close left
    // - Far left
    // - Close Right
    // - Far Right        

    protected SimpleOctree m_Owner;

    protected int m_Depth;
    protected bool m_IsLeaf;
    public Bounds WorldBounds { get; protected set; }

    protected SimpleOctreeNode[] m_Children;
    protected List<IOctreeContent> m_Contents;



    public SimpleOctreeNode(SimpleOctree owner, Bounds bounds, int depth) {
        this.m_Owner = owner;
        WorldBounds = bounds;
        this.m_Depth = depth;

        Assert.Test(depth < owner.GetMaxDepth());
        m_IsLeaf = (depth >= owner.GetMaxDepth() - 1);

        if(m_IsLeaf) {
            m_Contents = new List<IOctreeContent>();
        } else {
            m_Children = new SimpleOctreeNode[8];
            m_Children[0] = new SimpleOctreeNode(m_Owner, CreateChildBounds(-1, -1, -1), depth + 1);
            m_Children[1] = new SimpleOctreeNode(m_Owner, CreateChildBounds(-1, -1,  1), depth + 1);
            m_Children[2] = new SimpleOctreeNode(m_Owner, CreateChildBounds( 1, -1, -1), depth + 1);
            m_Children[3] = new SimpleOctreeNode(m_Owner, CreateChildBounds( 1, -1,  1), depth + 1);
            m_Children[4] = new SimpleOctreeNode(m_Owner, CreateChildBounds(-1,  1, -1), depth + 1);
            m_Children[5] = new SimpleOctreeNode(m_Owner, CreateChildBounds(-1,  1,  1), depth + 1);
            m_Children[6] = new SimpleOctreeNode(m_Owner, CreateChildBounds( 1,  1, -1), depth + 1);
            m_Children[7] = new SimpleOctreeNode(m_Owner, CreateChildBounds( 1,  1,  1), depth + 1);
        }
    }



    // Creates the bounds for a child. x, y and z must be either 1 or -1
    protected Bounds CreateChildBounds(int x, int y, int z) {
        Assert.Test(
            ((x == -1) || ( x == 1)) && ((y == -1) || ( y == 1)) && ((z == -1) || ( z == 1))
        );
        return(new Bounds(
            WorldBounds.center + new Vector3(
                WorldBounds.extents.x * x, WorldBounds.extents.y * y, WorldBounds.extents.z * z
            ) * 0.5f,
            WorldBounds.extents
        ));

    }



    public bool Insert(IOctreeContent content) {
        Assert.Test(content != null);
        if(m_IsLeaf) {
            m_Contents.Add(content);
            return(true);
        } else {
            bool inserted = false;
            for(int i = 0; i < m_Children.Length; ++i) {
                if(content.WorldBounds.Intersects(m_Children[i].WorldBounds)) {
                    if(m_Children[i].Insert(content)) inserted = true;
                }
            }
            return(inserted);
        }
    }



    public bool Remove(IOctreeContent content) {
        if(m_IsLeaf) {
            m_Contents.Remove(content);
            return(true);
        } else {
            bool removed = false;
            for(int i = 0; i < m_Children.Length; ++i) {
                if(content.WorldBounds.Intersects(m_Children[i].WorldBounds)) {
                    if(m_Children[i].Remove(content)) removed = true;
                }
            }
            return(removed);
        }
    }



    public bool Intersects(Bounds worldBounds) {
        return(this.WorldBounds.Intersects(worldBounds));
    }



    public bool IntersectsRay(Ray ray) {
        return(this.WorldBounds.IntersectRay(ray));
    }



    public bool Raycast(Ray ray, float distance, out OctreeRaycastHit hit, int mask) {
        bool didHit = false;
        hit = new OctreeRaycastHit(float.MaxValue, Vector3.zero, null);

        if(m_IsLeaf) {
            for(int i = 0; i < m_Contents.Count; ++i) {
                IOctreeContent content = m_Contents[i];

                float d;
                if(content.IntersectRay(ray, out d) && d < hit.Distance && d < distance) {
                    hit = new OctreeRaycastHit(d, ray.origin + ray.direction.normalized * d, content);
                    didHit = true;
                }
            }
        } else {
            for(int i = 0; i < m_Children.Length; ++i) {
                if(!m_Children[i].IntersectsRay(ray)) continue;

                OctreeRaycastHit childHit;
                m_Children[i].Raycast(ray, distance, out childHit, mask);

                if(childHit.Distance < hit.Distance) {
                    didHit = true;
                    hit = childHit;
                }
            }
        }

        return(didHit);
    }



    private static Material m_LineMaterial;
    public void OnPostRender() {
        if(m_LineMaterial == null) {
            m_LineMaterial = new Material(
                "Shader \"Lines/Colored Blended\" {" +
                "SubShader { Pass { " +
                "    Blend SrcAlpha OneMinusSrcAlpha " +
                "    ZWrite Off Cull Off Fog { Mode Off } " +
                "    BindChannels {" +
                "      Bind \"vertex\", vertex Bind \"color\", color }" +
                "} } }"
                );
            m_LineMaterial.hideFlags = HideFlags.HideAndDontSave;
            m_LineMaterial.shader.hideFlags = HideFlags.HideAndDontSave;
        }
        
        m_LineMaterial.SetPass(0);
        
        GL.Begin(GL.LINES);
        GL.Color(new Color(1.0f, 0.0f, 1.0f, 0.5f));
        DrawCube(this.WorldBounds.center, this.WorldBounds.size);
        GL.End();
        
        if(!m_IsLeaf) {
            for(int i = 0; i < 8; ++i) {
                m_Children[i].OnPostRender();
            }
        }// else {
//            GL.Color(new Color(0.5f, 0.5f, 1.0f, 0.5f));
//            for(int i = 0; i < m_Contents.Count; ++i) {
//                DrawCube(m_Contents[i].Bounds.center, m_Contents[i].Bounds.size);
//            }
//            GL.End();
//        }
    }
    
    
    
    static private void DrawCube(Vector3 center, Vector3 size) {
        Vector3 halfSize = size * 0.5f;
        
        Vector3 p1 = center + new Vector3(-halfSize.x, -halfSize.y, -halfSize.z);
        Vector3 p2 = center + new Vector3(-halfSize.x, -halfSize.y,  halfSize.z);
        Vector3 p3 = center + new Vector3( halfSize.x, -halfSize.y,  halfSize.z);
        Vector3 p4 = center + new Vector3( halfSize.x, -halfSize.y, -halfSize.z);
        Vector3 p5 = center + new Vector3(-halfSize.x,  halfSize.y, -halfSize.z);
        Vector3 p6 = center + new Vector3(-halfSize.x,  halfSize.y,  halfSize.z);
        Vector3 p7 = center + new Vector3( halfSize.x,  halfSize.y,  halfSize.z);
        Vector3 p8 = center + new Vector3( halfSize.x,  halfSize.y, -halfSize.z);
        
        GL.Vertex(p1);
        GL.Vertex(p2);
        GL.Vertex(p2);
        GL.Vertex(p3);
        GL.Vertex(p3);
        GL.Vertex(p4);
        GL.Vertex(p4);
        GL.Vertex(p1);
        
        GL.Vertex(p5);
        GL.Vertex(p6);
        GL.Vertex(p6);
        GL.Vertex(p7);
        GL.Vertex(p7);
        GL.Vertex(p8);
        GL.Vertex(p8);
        GL.Vertex(p5);
        
        GL.Vertex(p1);
        GL.Vertex(p5);
        GL.Vertex(p2);
        GL.Vertex(p6);
        GL.Vertex(p3);
        GL.Vertex(p7);
        GL.Vertex(p4);
        GL.Vertex(p8);
    }

}
