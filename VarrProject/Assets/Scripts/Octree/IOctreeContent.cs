﻿using UnityEngine;
using System.Collections;



/// <summary>
/// Interface for octree contents. Please be sure to never change Bounds
/// or WorldBounds if the content is registered within a tree!
/// </summary>
public interface IOctreeContent {
    Bounds Bounds { get; }
	Bounds WorldBounds { get; }
	bool IntersectRay(Ray ray, out float distance);
}
