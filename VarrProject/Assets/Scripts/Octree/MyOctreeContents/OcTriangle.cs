﻿using UnityEngine;
using System.Collections;



public struct OcTriangle : IOctreeContent, System.IEquatable<OcTriangle> {
    public readonly Vector3 a;
	public readonly Vector3 b;
	public readonly Vector3 c;

	public Bounds Bounds { get; private set;}
	public Bounds WorldBounds { get; private set; }

	public OcTriangle(Vector3 a, Vector3 b, Vector3 c) {
		this.a = a;
		this.b = b;
		this.c = c;

		Vector3 min = Vector3.Min(a, b);
		min = Vector3.Min(min, c);

		Vector3 max = Vector3.Max(a, b);
		max = Vector3.Max(max, c);

		this.Bounds =      new Bounds((max + min) * 0.5f, (max - min));
		this.WorldBounds = new Bounds((max + min) * 0.5f, (max - min));
	}



	public const float EPILSON = 0.001f;
	public bool IntersectRay(Ray ray, out float distance) {
        return(MyMath3D.RayTriIntersectTrumbore(ray, a, b, c, EPILSON, out distance));
	}



    public const float Epilson = 0.001f;
    public bool VectorEquals(Vector3 a, Vector3 b) {
        Vector3 final = a - b;
        return(
            Mathf.Abs(final.x) <= Epilson &&
            Mathf.Abs(final.y) <= Epilson &&
            Mathf.Abs(final.z) <= Epilson
        );
    }



    public override bool Equals(object obj) {
        if(!(obj is OcTriangle)) return(false);
        return(Equals((OcTriangle) obj));
    }



    public bool Equals(OcTriangle other) {
        return(VectorEquals(a, other.a) && VectorEquals(b, other.b) && VectorEquals(c, other.c));
    }



    static public bool operator ==(OcTriangle x, OcTriangle y) {
        return(x.Equals(y));
    }



    static public bool operator !=(OcTriangle x, OcTriangle y) {
        return(!x.Equals(y));
    }

    
    
    public override int GetHashCode() {
        return(a.GetHashCode() ^ b.GetHashCode() ^ c.GetHashCode());
    }
}
