﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;



public class OctreeNode {
    private Octree m_Octree;

	private bool m_IsLeaf;
	private int m_Depth;
	private int m_MaxDepth;
	private Bounds m_CurrentBounds;

	private OctreeNode[] m_Children;
	private List<IOctreeContent> m_Contents;

    public List<IOctreeContent> Contents { get { return(m_Contents); } }
	public Bounds Bounds { get; private set; }



	public OctreeNode() {
		m_Children = new OctreeNode[8];
		m_Contents = new List<IOctreeContent>();
	}



    public OctreeNode Init(Octree octree, Bounds bounds, int depth, int maxDepth) {
        this.m_Octree = octree;

        m_Depth = depth;
        m_MaxDepth = maxDepth;
        m_IsLeaf = true;
        m_CurrentBounds = bounds;

        m_Contents.Clear();
        this.Bounds = bounds;

        return(this);
    }



	public void Add(IOctreeContent content) {
		Bounds worldBounds = content.WorldBounds;

		if(!this.Bounds.Intersects(worldBounds)) {
			Assert.Throw("Trying to add 'content' outside the octree");
			return;
		}

		if((m_IsLeaf && m_Contents.Count == 0) || m_Depth == m_MaxDepth) {
			// Add if empty or at the max depth
			m_Contents.Add(content);
		} else {
			if(
				worldBounds.center.Equals(this.Bounds.center) ||
				(m_Contents.Count > 0 && m_Contents.TrueForAll(i => worldBounds.center.Equals(i.WorldBounds.center)))
			) {
				// I dont get this conditional!!!
				m_Contents.Add(content);
			} else {
				if(m_IsLeaf) {
				// Build children
					m_IsLeaf = false;
					Vector3 extents = this.Bounds.extents * 0.5f;

                    m_Children[0] = m_Octree.SpawnNode().Init(m_Octree, new Bounds(this.Bounds.center + new Vector3(-extents.x, -extents.y, -extents.z), this.Bounds.extents), m_Depth+1, m_MaxDepth);
                    m_Children[1] = m_Octree.SpawnNode().Init(m_Octree, new Bounds(this.Bounds.center + new Vector3(-extents.x, -extents.y,  extents.z), this.Bounds.extents), m_Depth+1, m_MaxDepth);
                    m_Children[2] = m_Octree.SpawnNode().Init(m_Octree, new Bounds(this.Bounds.center + new Vector3( extents.x, -extents.y, -extents.z), this.Bounds.extents), m_Depth+1, m_MaxDepth);
                    m_Children[3] = m_Octree.SpawnNode().Init(m_Octree, new Bounds(this.Bounds.center + new Vector3( extents.x, -extents.y,  extents.z), this.Bounds.extents), m_Depth+1, m_MaxDepth);
                    m_Children[4] = m_Octree.SpawnNode().Init(m_Octree, new Bounds(this.Bounds.center + new Vector3(-extents.x,  extents.y, -extents.z), this.Bounds.extents), m_Depth+1, m_MaxDepth);
                    m_Children[5] = m_Octree.SpawnNode().Init(m_Octree, new Bounds(this.Bounds.center + new Vector3(-extents.x,  extents.y,  extents.z), this.Bounds.extents), m_Depth+1, m_MaxDepth);
                    m_Children[6] = m_Octree.SpawnNode().Init(m_Octree, new Bounds(this.Bounds.center + new Vector3( extents.x,  extents.y, -extents.z), this.Bounds.extents), m_Depth+1, m_MaxDepth);
                    m_Children[7] = m_Octree.SpawnNode().Init(m_Octree, new Bounds(this.Bounds.center + new Vector3( extents.x,  extents.y,  extents.z), this.Bounds.extents), m_Depth+1, m_MaxDepth);

					// Move any existing items
					IOctreeContent[] contentsArray = m_Contents.ToArray();
					for(int i = 0; i < contentsArray.Length; ++i) {
						Bounds iWorldBounds = contentsArray[i].WorldBounds;

						if(!iWorldBounds.center.Equals(this.Bounds.center)) {
							for(int j = 0; j < 8; ++j) {
								if(m_Children[j].Intersects(iWorldBounds)) m_Children[j].Add(contentsArray[i]);
							}
							m_Contents.Remove(contentsArray[i]);
						}
					}
				}

				// Add new item into child
				for(int j = 0; j < 8; ++j) {
					if(m_Children[j].Intersects(worldBounds)) m_Children[j].Add(content);
				}
			}
		}
	}



    public void Remove(IOctreeContent content) {
        bool found = false;
        for(int i = 0; i < m_Contents.Count; ++i) {
            if(m_Contents[i] == content) {
                found = true;
                break;
            }
        }

        if(found) m_Contents.Remove(content);

        if(!IsLeaf()) {
            int emptyChildren = 0;
            bool hasFloater = false;
            List<IOctreeContent> floaters = null;

            for(int i = 0; i < 8; ++i) {
                m_Children[i].Remove(content);

                if(m_Children[i].IsEmpty()) {
                    ++emptyChildren;
                } else if(m_Children[i].IsLeaf()) {
                    if(floaters == null) {
                        floaters = m_Children[i].Contents; // TODO : SO OPTIMIZE THIS!
                    } else if(!floaters.Equals(m_Children[i].Contents)) {
                        hasFloater = false;
                    }
                } else {
                    hasFloater = false;
                }

                if(emptyChildren == 8) {
                    m_IsLeaf = true;

                    for(int j = 0; j < 8; ++j) {
                        if(m_Children[j] != null) {
                            m_Octree.AddToPool(m_Children[j]);
                            m_Children[j] = null;
                        }
                    }
                } else if(hasFloater && m_Contents.Count == 0) {
                    for(int j = 0; j < floaters.Count; ++j) {
                        m_Contents.Add(floaters[j]);
                        m_IsLeaf = true;
                    }

                    for(int j = 0; j < 8; ++j) {
                        if(m_Children[j] != null) {
                            m_Octree.AddToPool(m_Children[j]);
                            m_Children[j] = null;
                        }
                    }
                }
            }
        }
    }



	public bool IsEmpty() {
		return(m_IsLeaf && m_Contents.Count == 0);
	}



	public bool IsLeaf() {
		return(m_IsLeaf);
	}



	public bool Intersects(Bounds bounds) {
		return(m_CurrentBounds.Intersects(bounds));
	}



	public bool IntersectsRay(Ray ray) {
		return(m_CurrentBounds.IntersectRay(ray));
	}



	public bool Raycast(Ray ray, float distance, out OctreeRaycastHit hit, int mask) {
		bool didHit = false;
		hit = new OctreeRaycastHit(float.MaxValue, Vector3.zero, null);

		if(!m_IsLeaf) {
			var children = new List<KeyValuePair<float, OctreeNode>>(8);

			for(int i = 0; i < 8; ++i) {
				float d;
				if(!m_Children[i].IsEmpty() && m_Children[i].Bounds.IntersectRay(ray, out d) && d < distance) {
					children.Add(new KeyValuePair<float, OctreeNode>(d, m_Children[i]));
				}
			}

			children.Sort(delegate(KeyValuePair<float, OctreeNode> x, KeyValuePair<float, OctreeNode> y) {
				return(x.Key.CompareTo(y.Key));
			} );

			for(int i = 0; i < children.Count; ++i) {
				if(hit.Distance < children[i].Key) break;

				OctreeRaycastHit h;
				if(children[i].Value.Raycast(ray, distance, out h, mask) && h.Distance < hit.Distance && h.Distance < distance) {
					hit = h;
					didHit = true;
				}
			}
		}

		// Now cast against node items
		for(int i = 0;  i < m_Contents.Count; ++i) {
			IOctreeContent content = m_Contents[i];
			float d;
			if(content.IntersectRay(ray, out d) && d < hit.Distance && d < distance) {
				hit = new OctreeRaycastHit(d, ray.origin + ray.direction.normalized * d, content);
				didHit = true;
			}
		}

		return(didHit);
	}



	private static Material m_LineMaterial;
	public void OnPostRender() {
		if(m_LineMaterial == null) {
			m_LineMaterial = new Material(
				"Shader \"Lines/Colored Blended\" {" +
			    "SubShader { Pass { " +
			    "    Blend SrcAlpha OneMinusSrcAlpha " +
			    "    ZWrite Off Cull Off Fog { Mode Off } " +
			    "    BindChannels {" +
			    "      Bind \"vertex\", vertex Bind \"color\", color }" +
			    "} } }"
			);
			m_LineMaterial.hideFlags = HideFlags.HideAndDontSave;
			m_LineMaterial.shader.hideFlags = HideFlags.HideAndDontSave;
		}

		m_LineMaterial.SetPass(0);

		GL.Begin(GL.LINES);
			GL.Color(new Color(1.0f, 0.0f, 1.0f, 0.5f));
			DrawCube(this.Bounds.center, this.Bounds.size);
		GL.End();

		if(!m_IsLeaf) {
			for(int i = 0; i < 8; ++i) {
				m_Children[i].OnPostRender();
			}
		}
	}



	static private void DrawCube(Vector3 center, Vector3 size) {
		Vector3 halfSize = size * 0.5f;

		Vector3 p1 = center + new Vector3(-halfSize.x, -halfSize.y, -halfSize.z);
		Vector3 p2 = center + new Vector3(-halfSize.x, -halfSize.y,  halfSize.z);
		Vector3 p3 = center + new Vector3( halfSize.x, -halfSize.y,  halfSize.z);
		Vector3 p4 = center + new Vector3( halfSize.x, -halfSize.y, -halfSize.z);
		Vector3 p5 = center + new Vector3(-halfSize.x,  halfSize.y, -halfSize.z);
		Vector3 p6 = center + new Vector3(-halfSize.x,  halfSize.y,  halfSize.z);
		Vector3 p7 = center + new Vector3( halfSize.x,  halfSize.y,  halfSize.z);
		Vector3 p8 = center + new Vector3( halfSize.x,  halfSize.y, -halfSize.z);

		GL.Vertex(p1);
		GL.Vertex(p2);
		GL.Vertex(p2);
		GL.Vertex(p3);
		GL.Vertex(p3);
		GL.Vertex(p4);
		GL.Vertex(p4);
		GL.Vertex(p1);
		
		GL.Vertex(p5);
		GL.Vertex(p6);
		GL.Vertex(p6);
		GL.Vertex(p7);
		GL.Vertex(p7);
		GL.Vertex(p8);
		GL.Vertex(p8);
		GL.Vertex(p5);
		
		GL.Vertex(p1);
		GL.Vertex(p5);
		GL.Vertex(p2);
		GL.Vertex(p6);
		GL.Vertex(p3);
		GL.Vertex(p7);
		GL.Vertex(p4);
		GL.Vertex(p8);
	}
}
