﻿using UnityEngine;



public class OctreeClosesPoint {
	public float Distance { get; private set; }
	public IOctreeContent Content { get; private set; }
	public OctreeNode Node { get; private set; }



	public OctreeClosesPoint(float distance, IOctreeContent content, OctreeNode node) {
		this.Distance = distance;
		this.Content = content;
		this.Node = node;
	}
}
