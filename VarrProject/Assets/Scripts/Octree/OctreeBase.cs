﻿using UnityEngine;
using System.Collections;



public abstract class OctreeBase {
    abstract public void Init(Bounds bounds, int maxDepth);

    abstract public IOctreeContent Insert(IOctreeContent content);
    abstract public IOctreeContent Remove(IOctreeContent content);

    abstract public  bool Raycast(Ray ray, float distance, int mask, out OctreeRaycastHit hit);

    abstract public int GetMaxDepth();

    abstract public void DebugDraw();
}
