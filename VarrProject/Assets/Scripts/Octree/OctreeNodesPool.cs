﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class OctreeNodesPool {
    protected Stack<OctreeNode> m_Pool;



    public OctreeNodesPool() {
        m_Pool = new Stack<OctreeNode>();
    }



    public void AddToPool(OctreeNode node) {
        m_Pool.Push(node);
    }



    public OctreeNode GetNewNode() {
        OctreeNode node = null;

        if(m_Pool.Count > 0) {
            node = m_Pool.Pop();
        } else {
            node = new OctreeNode();
        }

        return(node);
    }
}
