﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class SimpleFightersGroupsManagers : MonoBehaviour, IFightersGroupsManager {
	protected Dictionary<GroupClassifications, List<FightersGroupBehavior>> m_Groups;



	protected void Awake() {
		m_Groups = new Dictionary<GroupClassifications, List<FightersGroupBehavior>>();
		foreach(
			GroupClassifications classification in System.Enum.GetValues(typeof(GroupClassifications))
		) {
			m_Groups.Add(classification, new List<FightersGroupBehavior>());
		}
	}


	
	public void RegisterGroup(FightersGroupBehavior group) {
		Assert.Test(!m_Groups[group.Classification].Contains(group));
		Assert.Test(group.Classification != GroupClassifications.None);
		m_Groups[group.Classification].Add(group);
	}



	public void UnregisterGroup(FightersGroupBehavior group) {
		Assert.Test(m_Groups[group.Classification].Contains(group));
		Assert.Test(group.Classification != GroupClassifications.None);
		m_Groups[group.Classification].Remove(group);
	}



	public FightersGroupBehavior GetRandomTarget(GroupClassifications classification) {
		Assert.Test(classification != GroupClassifications.None);
		FightersGroupBehavior result = null;

		if(m_Groups[classification].Count > 0) {
			result = m_Groups[classification][Random.Range(0, m_Groups[classification].Count)];
		}

		return(result);
	}



	public FightersGroupBehavior GetClosestTarget(
		GroupClassifications classification,
		Vector3 position, float range
	) {
		Assert.Test(classification != GroupClassifications.None);
		FightersGroupBehavior result= null;

		float sqrRange = range * range;
		float sqrClosest = sqrRange;
		for(int i = 0; i < m_Groups[classification].Count; ++i) {
			Vector3 otherPos = m_Groups[classification][i].transform.position;
			Vector3 diff = otherPos - position;
			diff.y = 0;
			float dist = diff.sqrMagnitude;

			if(dist < sqrClosest) {
				sqrClosest = dist;
				result = m_Groups[classification][i];
			}
		}

		return(result);
	}
}
