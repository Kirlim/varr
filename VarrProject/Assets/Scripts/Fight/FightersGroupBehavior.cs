﻿using UnityEngine;
using System.Collections;



/// <summary>
/// Represents a group of fighters. This allows a party to be
/// defeated only when all members are down.
/// </summary>
[RequireComponent(typeof(MovementBehavior))]
public class FightersGroupBehavior : MonoBehaviour {
	[SerializeField] [Immutable] protected GroupClassifications m_Classification;
	[SerializeField] protected FighterBehavior[] m_Fighters;

	public bool Engaged { get; protected set; }
	protected FightersGroupBehavior m_Target;

	[Tooltip("When following a target to attack, give up if it get this far")]
	public float maxHuntDist;

	[Tooltip("Should move to attack nearby targets on sight?")]
	public bool aggressive;
	[Tooltip("How far can this group 'see' a target for aggressive hunting?")]
	public float aggroRange;
	[Tooltip("How far the group needs to be from target before attacking")]
	public float attackRange;

	public GroupClassifications Classification { get { return(m_Classification); } }
	protected MovementBehavior m_Movement;



	protected void Awake() {
		Engaged = false;
		m_Movement = this.GetComponent<MovementBehavior>();
	}



	protected void OnEnable() {
		Global.FightersGroupsManager.RegisterGroup(this);
	}
	
	
	
	protected void OnDisable() {
		Global.FightersGroupsManager.UnregisterGroup(this);
	}


	
	protected void Update() {
		if(!this.IsAlive()) {
			// TODO : MORE SUBTLE WAY TO DIE!
			GameObject.Destroy(this.gameObject);
		}



		if(Engaged) {
			if(m_Target != null) {
				if(!IsTargetWithinAttackRange()) {
					Engaged = false;
					if(m_Movement.ForcedHalt) m_Movement.LiftForcedHalt();
                } else if(!m_Movement.ForcedHalt) {
					m_Movement.ForceHalt();
				}
			} else {
				Engaged = false;
			}
		} else {
			if(m_Movement.ForcedHalt) m_Movement.LiftForcedHalt();

			if(aggressive) {
				if(m_Target == null) {
					m_Target = FindNewTarget();
				} else {
					if(!Engaged) {
						PursueTarget();
						if(IsTargetWithinAttackRange()) {
							Engaged = true;
							m_Target.GotAttacked(this);
						}
					}
				}
			}

//			if(m_Classification == GroupClassifications.Monsters) {
//				if(m_Target == null) {
//					m_Target = FindNewTarget();
//				} else {
//					if(!Engaged) {
//						PursueTarget();
//						if(HeuristicDistanceToTarget() <= Global.Map.TileSize / 2) {
//							// TODO : USE RANGE
//							Engaged = true;
//							m_Target.GotAttacked(this);
//						}
//					}
//				}
//			}
		}
	}



	public void GotAttacked(FightersGroupBehavior other) {
		if(m_Target == null) {
			m_Target = other;
			if(IsTargetWithinAttackRange()) {
				Engaged = true;
			}
		}
	}



	public void PursueTarget() {
		Assert.Test(m_Target != null);

		if(m_Movement.State == MovementStates.Idle ||  m_Movement.State == MovementStates.Moving) {
			m_Movement.MoveTo(Global.Map.GetPosAsTile(m_Target.transform.position), true);
		} else {
			m_Movement.FinishMovement();
        }
        
        //		if(m_Movement.State == MovementStates.Moving) return;
//		if(m_Movement.State == MovementStates.Idle) {
//			m_Movement.MoveTo(Global.Map.GetPosAsTile(m_Target.transform.position));
//		} else {
//			m_Movement.FinishMovement();
//		}
	}



	public bool ApplyAttackToRandomFighter(FightersGroupBehavior attacker, int damage) {
		bool applied = false;

		if(m_Fighters != null && m_Fighters.Length > 0) {
			int index = Random.Range(0, m_Fighters.Length);
			m_Fighters[index].ApplyDamage(damage);
			GotAttacked(attacker);
			applied = true;
		}

		return(applied);
	}



	public bool IsAlive() {
		bool alive = false;

		if(m_Fighters != null && m_Fighters.Length > 0) {
			for(int i = 0; i < m_Fighters.Length; ++i) {
				if(m_Fighters[i].IsAlive) {
					alive = true;
					break;
				}
			}
		}

		return(alive);
	}



	#region Targets and Detection

	public FightersGroupBehavior FindNewTarget() {
		GroupClassifications targetClass = GroupClassifications.None;
		switch(m_Classification) {
		case GroupClassifications.Monsters: targetClass = GroupClassifications.Player; break;
		case GroupClassifications.Player: targetClass = GroupClassifications.Monsters; break;
		}

		Assert.Test(targetClass != GroupClassifications.None);

		// TODO : CACHE FOR PERFORMANCE?
		return(Global.FightersGroupsManager.GetClosestTarget(
			targetClass, this.transform.position, aggroRange
		));
	}



	public FightersGroupBehavior GetCurrentTarget() {
		return(m_Target);
	}



	public int HeuristicDistanceToTarget() {
		// TODO : OPTIMIZE THIS
		Vector3 myPos = this.transform.position;
		Vector3 otherPos = m_Target.transform.position;
		float manhDist = Mathf.Abs(myPos.x - otherPos.x) + Mathf.Abs(myPos.z - otherPos.z);

		return(Mathf.FloorToInt(manhDist));
	}



	public bool IsTargetWithinAttackRange() {
		Assert.Test(m_Target != null);
		// TODO : OPTIMIZE THIS
		Vector3 myPos = this.transform.position;
		Vector3 otherPos = m_Target.transform.position;
		otherPos.y = myPos.y;

		float sqrDist = (otherPos - myPos).sqrMagnitude;
		float sqrAtkDist = attackRange * attackRange;

		return(sqrDist <= sqrAtkDist);
	}


	#endregion
}
