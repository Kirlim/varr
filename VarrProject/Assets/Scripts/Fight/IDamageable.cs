﻿using UnityEngine;
using System.Collections;



public interface IDamageable {
	void ApplyDamage(int damage);
	void ApplyHealing(int healing);
}

public class DmgMsg {
	public const string ApplyDamage = "ApplyDamage";
	public const string ApplyHealing = "ApplyHealing";
}
