﻿using UnityEngine;
using System.Collections;



/// <summary>
/// Single fighter of a group.
/// </summary>
public class FighterBehavior : MonoBehaviour, IDamageable {
	public int baseMaxHp;
	public int baseDamage;
	public int range;
	public float attackDelay;

	[SerializeField] protected int m_Hp;
	[SerializeField] [Immutable] protected FightersGroupBehavior m_Group;

	protected float m_CurrentAttackDelay;

	public bool IsAlive { get { return(m_Hp > 0); } }



	protected void Update() {
		if(m_Group.Engaged) {
			FightersGroupBehavior target = m_Group.GetCurrentTarget();
			if(target != null && m_CurrentAttackDelay < 0.0f) {
				if(target.ApplyAttackToRandomFighter(m_Group, baseDamage)) {
					m_CurrentAttackDelay = attackDelay;
				}
			} else {
				m_CurrentAttackDelay -= Time.deltaTime;
			}
		}
	}



	public void ApplyDamage(int damage) {
		m_Hp -= damage;
		CreateDamageIndicator(damage);
		RefreshHp();
	}



	public void ApplyHealing(int healing) {
		m_Hp += healing;
		RefreshHp();
	}



	protected void RefreshHp() {
		m_Hp = Mathf.Clamp(m_Hp, 0, baseMaxHp);
	}



	public void CreateDamageIndicator(int damage) {
		// TODO : GET RID OF MAGIC VALUE
		Vector3 indicatorPos = this.transform.position;
		indicatorPos.y += 10.0f;

		Global.DamageIndicators.InstantiateIndicator(damage, indicatorPos);
	}
}
