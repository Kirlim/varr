﻿using UnityEngine;
using System.Collections;



/// <summary>
/// Used to create damage hits indicators
/// </summary>
[AddComponentMenu("Scripts/Fight/Damage Indicators Manager")]
public class DamageIndicatorsManager : MonoBehaviour {
	public DamageIndicatorBehavior damageIndicatorPrefab;
	public Canvas indicatorsCanvas;



	public DamageIndicatorBehavior InstantiateIndicator(int damage, Vector3 position) {
		GameObject newGo = Instantiate(damageIndicatorPrefab.gameObject) as GameObject;
		DamageIndicatorBehavior indicator = newGo.GetComponent<DamageIndicatorBehavior>();
		newGo.transform.SetParent(indicatorsCanvas.transform);
		indicator.SetUp(damage, position);
		return(indicator);
	}
}
