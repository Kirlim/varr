﻿using UnityEngine;
using System.Collections;



/// <summary>
/// Damage indicator behavior. They must be UI
/// objects on viewport coordinates
/// </summary>
[AddComponentMenu("Scripts/Fight/Damage Indicators")]
public class DamageIndicatorBehavior : MonoBehaviour {
	public float minAngle, maxAngle;
	public float startSpeed;
	public float gravity;
	public float duration;

	public ValueUITextUtil valueTextRef;

	[Tooltip("If null, will use main camera instead")]
	public Camera showCamera;

	protected float m_Elapsed;
	protected Vector3 m_Speed;
	
	protected Vector3 m_WorldCoordinates;
	protected Vector3 m_ViewportCoordinates;
	protected Vector3 m_ScreenportCoordinates;
	protected RectTransform m_RTransform;




	protected void Awake() {
		Assert.Test(valueTextRef != null);
		m_RTransform = this.GetComponent<RectTransform>();
	}



	protected void Update() {
		Camera cam = showCamera;
		if(cam == null) cam = Camera.main;

		m_Speed.y -= gravity * Time.deltaTime;

		m_WorldCoordinates += m_Speed * Time.deltaTime;
		m_ScreenportCoordinates = cam.WorldToScreenPoint(m_WorldCoordinates);
		m_RTransform.position = m_ScreenportCoordinates;

		if(m_Elapsed >= duration) GameObject.Destroy(this.gameObject);
		m_Elapsed += Time.deltaTime;
	}



	public void SetUp(int damage, Vector3 position) {
		Camera cam = showCamera;
		if(cam == null) cam = Camera.main;

		m_WorldCoordinates = position;
		m_ScreenportCoordinates = cam.WorldToScreenPoint(position);
		m_RTransform.position = m_ScreenportCoordinates;

		float angle = Random.Range(minAngle, maxAngle) * Mathf.Deg2Rad;
		m_Speed = new Vector2(
			Mathf.Cos(angle) * startSpeed,
			Mathf.Sin(angle) * startSpeed
		);

		valueTextRef.SetText(damage.ToString());
	}
}
