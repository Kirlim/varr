﻿using UnityEngine;
using System.Collections;



public interface IFightersGroupsManager {
	void RegisterGroup(FightersGroupBehavior group);
	void UnregisterGroup(FightersGroupBehavior group);

	FightersGroupBehavior GetRandomTarget(GroupClassifications classification);
	FightersGroupBehavior GetClosestTarget(
		GroupClassifications classification, Vector3 position, float range
	);
}
