﻿using UnityEngine;

[System.Serializable]
public struct IntVector2 : System.IEquatable<IntVector2> {
	public int x, y;


	public IntVector2(int x, int y) {
		this.x = x;
		this.y = y;
	}



    static public IntVector2 operator +(IntVector2 l, IntVector2 r) {
        return(new IntVector2(l.x + r.x, l.y + r.y));
    }

    static public IntVector2 operator -(IntVector2 l, IntVector2 r) {
        return(new IntVector2(l.x - r.x, l.y - r.y));
    }



	public override bool Equals(object obj) {
		if(!(obj is IntVector2)) return(false);
		return(Equals((IntVector2) obj));
	}



	public bool Equals(IntVector2 other) {
		return(x == other.x && y == other.y);
	}
	
	
	
	static public bool operator ==(IntVector2 l, IntVector2 r) {
		return(l.Equals(r));
	}

	static public bool operator !=(IntVector2 l, IntVector2 r) {
		return(!l.Equals(r));
	}
	
	
	
	public override int GetHashCode() {
		return(x.GetHashCode() ^ y.GetHashCode());
	}
	
	
	
	static readonly public IntVector2 zero = new IntVector2(0, 0);
}
