﻿using UnityEngine;
using System.Collections;



public interface IFarm {
	bool HasWork { get; }

	void Tick();
	FarmWorks GetWork();
	void Reap();

	Storage Crop { get; }

	IntVector2 GetTilePos();
}
