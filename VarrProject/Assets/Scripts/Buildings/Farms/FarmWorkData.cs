﻿using UnityEngine;
using System.Collections;

public struct FarmWorkData : System.IEquatable<FarmWorkData> {
	readonly public IFarm farm;
	readonly public FarmerBehavior farmer;
	readonly public IGranary granary;
	readonly public int hash;



	public FarmWorkData(IFarm farm, FarmerBehavior farmer, IGranary granary) {
		this.farm = farm;
		this.farmer = farmer;
		this.granary = granary;
		this.hash =
			System.Runtime.CompilerServices.RuntimeHelpers.GetHashCode(farm) ^
			System.Runtime.CompilerServices.RuntimeHelpers.GetHashCode(farmer) ^
			System.Runtime.CompilerServices.RuntimeHelpers.GetHashCode(granary);
	}



	public override bool Equals(object obj) {
		if(!(obj is FarmWorkData)) return(false);
		return(Equals((FarmWorkData) obj));
	}



	public bool Equals(FarmWorkData other) {
		return(farm == other.farm && farmer == other.farmer && granary == other.granary);
	}



	static public bool operator ==(FarmWorkData x, FarmWorkData y) {
		return(x.Equals(y));
	}

	static public bool operator !=(FarmWorkData x, FarmWorkData y) {
		return(!x.Equals(y));
	}



	public override int GetHashCode() {
		return(hash);
	}



	static public FarmWorkData Empty = new FarmWorkData(null, null, null);
}