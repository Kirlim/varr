﻿using UnityEngine;
using System.Collections;



[AddComponentMenu("Scripts/Buildings/Farms/Farm Behavior")]
[RequireComponent(typeof(MapObject))]
public class FarmBehavior : MonoBehaviour, IFarm {
	public bool FullyGrown { get; protected set; }
	public bool NeedTreatment { get; protected set; }
	public bool HasWork { get { return(m_WorkNeeded != FarmWorks.None); } }

	public Storage crop;
	public Storage Crop { get { return(crop); } }

	[System.Serializable]
	public struct Stage {
		public Material material;
		public float height;
	}

	public Stage[] stages;
	public int minStageTicks;
	public float nextStageProbability;
	public int CurrentStage { get; protected set; }
	public int CurrentStageTicks { get; protected set; }

	protected FarmWorks m_WorkNeeded;

	public Transform meshRef;
	public Renderer meshRendererRef;
	public Transform shadowCasterRef;



	protected void Awake() {
		m_WorkNeeded = FarmWorks.None;
	}



	/// <summary>
	/// A tick is a chance to "grow" the farm
	/// </summary>
	public void Tick() {
		if(!(CurrentStage < stages.Length)) return;

		CurrentStageTicks++;
		if(CurrentStageTicks > minStageTicks) {

			float chance = Random.Range(0.0f, 1.0f);
			if(chance <= nextStageProbability) {
				if(m_WorkNeeded == FarmWorks.None) {
					SetStage(CurrentStage + 1);
				}
			}
		}
	}



	public FarmWorks GetWork() {
		return(m_WorkNeeded);
	}



	public void Reap() {
		SetStage(0);
        m_WorkNeeded = FarmWorks.None;
		Global.FarmsManager.UnregisterWork((IFarm) this);
	}



	public IntVector2 GetTilePos() {
		// TODO : CACHE THE TRANSFORM
		return(Global.Map.GetPosAsTile(this.transform.position));
	}



	protected void SetStage(int newStage) {
		newStage = Mathf.Clamp(newStage , 0, stages.Length - 1);
		CurrentStage = newStage;

		Vector3 scale = meshRef.localScale;
		scale.y = stages[CurrentStage].height;
		meshRef.localScale = scale;

		scale = shadowCasterRef.localScale;
		scale.y = stages[CurrentStage].height;
		shadowCasterRef.localScale = scale;

		meshRendererRef.sharedMaterial = stages[CurrentStage].material;
		FullyGrown = (CurrentStage == stages.Length - 1);

		if(FullyGrown && m_WorkNeeded == FarmWorks.None) {
			m_WorkNeeded = FarmWorks.Reap;
			Global.FarmsManager.RegisterWork(this);
		}

		CurrentStageTicks = 0;
	}
}
