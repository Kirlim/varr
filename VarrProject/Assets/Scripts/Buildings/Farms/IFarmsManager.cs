using UnityEngine;
using System.Collections;



public interface IFarmsManager {
    IFarm RegisterFarm(IFarm farm);
    IFarm UnregisterFarm(IFarm farm);

	IFarm RegisterWork(IFarm farm);
	IFarm UnregisterWork(IFarm farm);

	FarmWorkData GetWork(FarmerBehavior farmer, IntVector2 farmersHousePos, IntVector2 farmerPos);
	IGranary GetFarmRelatedGranary(IFarm farm);
}
