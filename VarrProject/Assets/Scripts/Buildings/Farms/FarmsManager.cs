using UnityEngine;
using System.Collections;
using System.Collections.Generic;



[AddComponentMenu("Scripts/Buildings/Farms/Farms Manager")]
public class FarmsManager : MonoBehaviour, IFarmsManager {
	[SerializeField] protected FarmBehavior farmPrefab;
	[SerializeField] protected float tickSeconds;
	protected float m_TickElapsed;

	protected List<IFarm> m_Farms;
	protected List<FarmWorkData> m_Work;



	protected void Awake() {
		Assert.Test(farmPrefab != null);
		m_Farms = new List<IFarm>();
		m_Work = new List<FarmWorkData>();
	}



	protected void Update() {
		m_TickElapsed += Time.deltaTime;
		while(m_TickElapsed >= tickSeconds) {
			for(int i = 0; i < m_Farms.Count; ++i) m_Farms[i].Tick();
			m_TickElapsed -= tickSeconds;
		}
	}



    public IFarm RegisterFarm(IFarm farm) {
        Assert.Test(farm != null);
        Assert.Test(!m_Farms.Contains(farm));
        m_Farms.Add(farm);
        return(farm);
    }



    public IFarm UnregisterFarm(IFarm farm) {
        Assert.Test(farm != null);
        Assert.Test(m_Farms.Contains(farm));
        m_Farms.Remove(farm);
        return(farm);
    }



	public IFarm RegisterWork(IFarm farm) {
		Assert.Test(m_Farms.Contains(farm) && farm != null && farm.HasWork);
		Assert.Test(m_Work.FindIndex(X => X.farm == farm) < 0);

		m_Work.Add(new FarmWorkData(farm, null, null));
		return(farm);
	}



	public IFarm UnregisterWork(IFarm farm) {
		Assert.Test(m_Farms.Contains(farm) && farm != null);
		Assert.Test(m_Work.FindIndex(X => X.farm == farm) >= 0);

		int index = -1;
		for(int i = 0; i < m_Work.Count; ++i) {
			if(m_Work[i].farm == farm) {
				index = i;
				break;
			}
		}

		if(index >= 0) {
			m_Work.RemoveAt(index);
		}

		return(farm);
	}



	public FarmWorkData GetWork(FarmerBehavior farmer, IntVector2 farmersHousePos, IntVector2 farmerPos) {
		int preference = int.MinValue;
		int bestFarmIndex = -1;

		for(int i = 0; i < m_Work.Count; ++i) {
			FarmWorkData work = m_Work[i];
			IFarm currentFarm = work.farm;
			IGranary granary = null;

			if(work.farmer == null) {
				IntVector2 farmPos = currentFarm.GetTilePos();

				if(currentFarm.GetWork() == FarmWorks.Reap) {
					// We need to reap the farm. Check that there is an granary
					// with enough space nearby
					granary = 
						Global.GranariesManager.GetClosestGranaryForStoring(farmPos, currentFarm.Crop);
					if(granary != null) {
						work = new FarmWorkData(work.farm, work.farmer, granary);
						m_Work[i] = work;
					} else {
						continue;
					}
				}

				int farmPreference = 
					- ((
						Mathf.Abs(farmerPos.x - farmPos.x) +
						Mathf.Abs(farmerPos.y - farmPos.y)
					)) - ((
						Mathf.Abs(farmersHousePos.x - farmPos.x) +
						Mathf.Abs(farmersHousePos.y - farmPos.y)
					));

				if(farmPreference > preference) {
					preference = farmPreference;
					bestFarmIndex = i;
				}
			}
		}


		FarmWorkData result = FarmWorkData.Empty;

		if(bestFarmIndex >= 0) {
			FarmWorkData workData = m_Work[bestFarmIndex];
			workData = new FarmWorkData(workData.farm, farmer, workData.granary);

			if(workData.granary != null) {
				Assert.Test(workData.farm.GetWork() == FarmWorks.Reap);
				workData.granary.ReserveStorage(workData.farm.Crop);
			}

			m_Work[bestFarmIndex] = workData;
			result = workData;
		}
		
		return(result);
	}



	public IGranary GetFarmRelatedGranary(IFarm farm) {
		Assert.Test(farm != null);
		IGranary granary = null;

		for(int i = 0; i < m_Work.Count; ++i) {
			if(m_Work[i].farm == farm) {
				granary = m_Work[i].granary;
				Assert.Test(granary != null);
				break;
			}
		}

		return(granary);
	}
}
