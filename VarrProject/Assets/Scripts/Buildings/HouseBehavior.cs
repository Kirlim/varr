﻿using UnityEngine;
using System.Collections;



/// <summary>
/// Simple House where people can live
/// </summary>
[RequireComponent(typeof(MapObject))]
public class HouseBehavior : MonoBehaviour {
	// Condition for populating
	public bool needRoads;

	[SerializeField] protected int m_MaxPop;
	public int MaxPop { get { return(m_MaxPop); } }
	virtual public int PopCount { get; protected set; }

	protected MapObject m_MapObject;



	virtual protected void Awake() {
		m_MapObject = this.GetComponent<MapObject>();
		Assert.Test(m_MapObject != null);
	}



	virtual protected void Update() {
		if(PopCount > 0) return;

		bool roads = false;
		if(m_MapObject.PosMin.y > 0) {
			for(int x = m_MapObject.PosMin.x; x <= m_MapObject.PosMax.x && roads == false; ++x) {
				if(Global.Map.IsRoad(x, m_MapObject.PosMin.y - 1)) roads = true;
			}
		}
		if(m_MapObject.PosMax.y < Global.Map.Height - 1) {
			for(int x = m_MapObject.PosMin.x; x <= m_MapObject.PosMax.x && roads == false; ++x) {
				if(Global.Map.IsRoad(x, m_MapObject.PosMax.y + 1)) roads = true;
			}
		}
		if(m_MapObject.PosMin.x > 0) {
			for(int y = m_MapObject.PosMin.y; y <= m_MapObject.PosMax.y && roads == false; ++y) {
				if(Global.Map.IsRoad(m_MapObject.PosMin.x - 1, y)) roads = true;
			}
		}
		if(m_MapObject.PosMax.x < Global.Map.Width - 1) {
			for(int y = m_MapObject.PosMin.y; y <= m_MapObject.PosMax.y && roads == false; ++y) {
				if(Global.Map.IsRoad(m_MapObject.PosMax.x + 1, y)) roads = true;
			}
		}

		if(needRoads && roads) {
			PopCount = m_MaxPop;
			OnNewPopCount(0, PopCount);
		}
	}

	virtual protected void OnNewPopCount(int previousCount, int newCount) {	}
}
