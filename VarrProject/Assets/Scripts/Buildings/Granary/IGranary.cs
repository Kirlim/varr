﻿using UnityEngine;
using System.Collections;



public interface IGranary : IBuilding {
    IntVector2 GetTilePos();
    bool CanStore(Storage storage);
	void ReserveStorage(Storage storage);
	void StoreReserved(Storage storage);
}
