using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class GranariesManager : MonoBehaviour, IGranariesManager {
    [SerializeField] protected int m_MaxGranarySearchDist;
    [SerializeField] protected GranaryBehavior m_GranaryPrefab;

    protected List<IGranary> m_Granaries;



    protected void Awake() {
        Assert.Test(m_GranaryPrefab != null);
        m_Granaries = new List<IGranary>();
    }



    public IGranary RegisterGranary(IGranary granary) {
        Assert.Test(granary != null);
        Assert.Test(!m_Granaries.Contains(granary));

        m_Granaries.Add(granary);
        return(granary);
    }



    public IGranary UnregisterGranary(IGranary granary) {
        Assert.Test(granary != null);
        Assert.Test(m_Granaries.Contains(granary));
        
        m_Granaries.Remove(granary);
        return(granary);
    }



    public IGranary GetClosestGranary(IntVector2 position) {
        IGranary granary = null;
        int sqrDist = int.MaxValue;

        for(int i = 0; i < m_Granaries.Count; ++i) {
            IntVector2 granaryTilePos = m_Granaries[i].GetTilePos();
            int xDist = Mathf.Abs(granaryTilePos.x - position.x);
            int zDist = Mathf.Abs(granaryTilePos.y - position.y);
            int thisSqrDist = xDist * xDist + zDist * zDist;

            if(thisSqrDist < sqrDist) {
                sqrDist = thisSqrDist;
                if(sqrDist < m_MaxGranarySearchDist * m_MaxGranarySearchDist) {
                    granary = m_Granaries[i];
                }
            }
        }

        return(granary);
    }



	public IGranary GetClosestGranaryForStoring(IntVector2 position, Storage storage) {
		IGranary granary = null;
		int sqrDist = int.MaxValue;
		
		for(int i = 0; i < m_Granaries.Count; ++i) {
			IntVector2 granaryTilePos = m_Granaries[i].GetTilePos();
			int xDist = Mathf.Abs(granaryTilePos.x - position.x);
			int zDist = Mathf.Abs(granaryTilePos.y - position.y);
			int thisSqrDist = xDist * xDist + zDist * zDist;
			
		   if(thisSqrDist < sqrDist && m_Granaries[i].CanStore(storage)) {
				sqrDist = thisSqrDist;
				if(sqrDist < m_MaxGranarySearchDist * m_MaxGranarySearchDist) {
					granary = m_Granaries[i];
				}
			}
		}
		
		return(granary);
	}
}
