using UnityEngine;
using System.Collections;



public interface IGranariesManager {
    IGranary RegisterGranary(IGranary granary);
    IGranary UnregisterGranary(IGranary granary);

    IGranary GetClosestGranary(IntVector2 position);
	IGranary GetClosestGranaryForStoring(IntVector2 position, Storage storage);
}
