﻿using UnityEngine;
using System.Collections;



public class GranaryBehavior : MonoBehaviour, IGranary {
    [SerializeField] protected StorageContainer m_Container;
	[SerializeField] protected HudBarBehavior m_StorageBarRef;



	protected void Awake() {
		Assert.Test(m_StorageBarRef != null);
	}



    public IntVector2 GetTilePos() {
        // TODO : CACHE THE TRANSFORM
        return(Global.Map.GetPosAsTile(this.transform.position));
    }



    public IntVector2 GetEntranceTilePos() {
        // TODO : CACHE THE TRANSFORM
        return(Global.Map.GetPosAsTile(this.transform.position + new Vector3(30, 0, 10)));
    }



    public bool CanStore(Storage storage) {
        return(m_Container.CanStore(storage));
    }



	public int GetReservedAmount(StorageData storageData) {
		return(m_Container.GetReservedAmount(storageData));
	}



	public void ReserveStorage(Storage storage) {
		Assert.Test(m_Container.GetFreeSpace() >= storage.quantity);
		m_Container.Reserve(storage.data, storage.quantity);
	}



	public void StoreReserved(Storage storage) {
		Assert.Test(storage.quantity <= m_Container.GetReservedAmount(storage.data));
		m_Container.StockReserved(storage.data, storage.quantity);
		m_StorageBarRef.SetPercentage(m_Container.GetStoredPercentage());
	}
}
