﻿using UnityEngine;
using System.Collections;



/// <summary>
/// Workers house is able to spawn workers when they get
/// populated or the previous workers die
/// </summary>
[AddComponentMenu("Scripts/Buildings/Workers House Behavior")]
public class WorkersHouseBehavior : HouseBehavior {
	public WorkerBehavior workerPrefab;
	protected WorkerBehavior m_CurrentWorker;



	protected override void OnNewPopCount(int previousCount, int newCount) {
		base.OnNewPopCount(previousCount, newCount);

		if(previousCount <= 0 && newCount > 0) {
			Assert.Test(m_CurrentWorker == null);
			GameObject newWorker = Instantiate(workerPrefab.gameObject) as GameObject;
			newWorker.gameObject.SetActive(true);
			m_CurrentWorker = newWorker.GetComponent<WorkerBehavior>();
			m_CurrentWorker.SetHome(this);

			m_CurrentWorker.transform.position = this.transform.position;
		}
	}
}
