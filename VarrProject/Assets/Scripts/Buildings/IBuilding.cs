﻿using UnityEngine;
using System.Collections;



public interface IBuilding {
    IntVector2 GetEntranceTilePos();
}
