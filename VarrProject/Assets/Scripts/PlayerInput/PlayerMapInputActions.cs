﻿/// <summary>
/// Actions that the player can do when clicking on the map
/// </summary>
public enum PlayerMapInputActions {
	None, BuildDirtRoad, BuildFarmersHouse, BuildFarm, BuildGranary, BuildGuardsHouse
}