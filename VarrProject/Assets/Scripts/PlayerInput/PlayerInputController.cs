using UnityEngine;
using System.Collections;



public class PlayerInputController : MonoBehaviour {
	public Map mapRef;

	private PlayerMapInputActions m_PlayerMapAction = PlayerMapInputActions.None;

	public Transform cameraTransform;

	public MapObject farmersHouseGuide;
	public MapObject farmGuide;
    public MapObject granaryGuide;
	public MapObject guardsHouseGuide;

    public MapObject farmersHousePrefab;
	public MapObject guardsHousePrefab;

	public float cameraMoveSpeed;
	public float cameraRotateSpeed;


	// TODO : DELETE LATER
	public GameObject slimePrefab;



	private void Awake() {
		farmersHouseGuide.gameObject.SetActive(false);
		guardsHouseGuide.gameObject.SetActive(false);
		farmGuide.gameObject.SetActive(false);
	}



	private void Update() {
		MoveCamera();

		UnityEngine.Profiling.Profiler.BeginSample("Map Raycasting");
		bool hasTile = false;
		int tx = -1, ty = -1;

		// TODO : CHECK IF I NEED TO CONFIRM THAT THE MOUSE IS WITHIN THE VIEWPORT
        Vector3 mouseVP = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        if(mouseVP.x <= 0.0f || mouseVP.y <= 0.0f || mouseVP.x >= 1.0f || mouseVP.y >= 1.0f) return;

		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		OctreeRaycastHit hit;

		if(mapRef.terrainOctree.Raycast(ray, float.MaxValue, 0x0, out hit)) {
			Vector3 pos = hit.Content.WorldBounds.center;
			tx = (int) (pos.x + mapRef.TileSize * 0.3f) / (int) mapRef.TileSize;
			ty = (int) (pos.z + mapRef.TileSize * 0.3f) / (int) mapRef.TileSize;
			hasTile = true;
		}
		UnityEngine.Profiling.Profiler.EndSample();



		bool leftClick = false;
//		bool rightClick = false;
		if(
			Input.GetMouseButton(0) &&
			!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()
		) {
			leftClick = true;
		}

//		if(
//			Input.GetMouseButton(1) &&
//			!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()
//			) {
//			rightClick = true;
//		}



		UnityEngine.Profiling.Profiler.BeginSample("Player Map Command");
		if(hasTile) {
			switch(m_PlayerMapAction) {
			case PlayerMapInputActions.BuildDirtRoad:
                IntVector2 pos = new IntVector2(tx, ty);
                if(leftClick) {
                    if(mapRef.CanPlaceRoad(pos.x, pos.y)) mapRef.BuildRoad(tx, ty); 
                }
				break;

			case PlayerMapInputActions.BuildFarmersHouse:
				farmersHouseGuide.SetPosition(mapRef, tx, ty);
				if(leftClick) {
                    Global.MapObjectsManager.Place(
                        MOIds.FarmersHouse, new IntVector2(tx, ty), Orientations.East
                    );
				}
				break;

			case PlayerMapInputActions.BuildFarm:
				farmGuide.SetPosition(mapRef, tx, ty);
				if(leftClick) {
                    Global.MapObjectsManager.Place(
                        MOIds.WheatFarm, new IntVector2(tx, ty), Orientations.East
                    );
				}
				break;

             case PlayerMapInputActions.BuildGranary:
                granaryGuide.SetPosition(mapRef, tx, ty);
                if(leftClick) {
                    Global.MapObjectsManager.Place(
                        MOIds.Granary, new IntVector2(tx, ty), Orientations.East
                    );
                }
                break;

			case PlayerMapInputActions.BuildGuardsHouse:
				guardsHouseGuide.SetPosition(mapRef, tx, ty);
				if(leftClick) {
                    Global.MapObjectsManager.Place(
                        MOIds.GuardsHouse, new IntVector2(tx, ty), Orientations.East
                    );
				}
				break;
			}
		}

		if(Input.GetMouseButtonDown(1)) {
			if(mapRef.IsFree(tx, ty)) {
				GameObject slime = Instantiate(slimePrefab) as GameObject;
				float y = mapRef.GetTileHeight(tx, ty);
				slime.transform.position = new Vector3(
					tx * mapRef.TileSize,
					y,
					ty * mapRef.TileSize
				);
			}
		}

		UnityEngine.Profiling.Profiler.EndSample();
	}



	public void SwitchPlayerAction(PlayerMapInputActions newAction) {
		switch(m_PlayerMapAction) {
		case PlayerMapInputActions.BuildFarmersHouse:
			farmersHouseGuide.gameObject.SetActive(false); break;
		case PlayerMapInputActions.BuildFarm:
			farmGuide.gameObject.SetActive(false); break;
        case PlayerMapInputActions.BuildGranary:
            granaryGuide.gameObject.SetActive(false); break;
		case PlayerMapInputActions.BuildGuardsHouse:
			guardsHouseGuide.gameObject.SetActive(false); break;
		}

		m_PlayerMapAction = newAction;

		switch(m_PlayerMapAction) {
		case PlayerMapInputActions.BuildFarmersHouse:
			farmersHouseGuide.gameObject.SetActive(true); break;
		case PlayerMapInputActions.BuildFarm:
			farmGuide.gameObject.SetActive(true); break;
        case PlayerMapInputActions.BuildGranary:
            granaryGuide.gameObject.SetActive(true); break;
		case PlayerMapInputActions.BuildGuardsHouse:
			guardsHouseGuide.gameObject.SetActive(true); break;
		}
	}



	private void MoveCamera() {
		Vector3 pos = cameraTransform.position;
		Vector3 angles = new Vector3(45.0f, 0.0f, 0.0f);
		angles.y = cameraTransform.eulerAngles.y;

		Vector3 movement = Vector3.zero;

		if(Input.GetKey(KeyCode.A)) movement.x -= cameraMoveSpeed * Time.deltaTime;
		if(Input.GetKey(KeyCode.D)) movement.x += cameraMoveSpeed * Time.deltaTime;
		if(Input.GetKey(KeyCode.W)) movement.z += cameraMoveSpeed * Time.deltaTime;
		if(Input.GetKey(KeyCode.S)) movement.z -= cameraMoveSpeed * Time.deltaTime;

		float angle = angles.y * Mathf.Deg2Rad;
		pos.x += movement.x * Mathf.Cos(-angle) - movement.z * Mathf.Sin(-angle);
		pos.z += movement.x * Mathf.Sin(-angle) + movement.z * Mathf.Cos(-angle);

		if(Input.GetKey(KeyCode.Q)) angles.y -= cameraRotateSpeed * Time.deltaTime;
		if(Input.GetKey(KeyCode.E)) angles.y += cameraRotateSpeed * Time.deltaTime;

		cameraTransform.position = pos;
		cameraTransform.eulerAngles = angles;
	}
}
