﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



[System.Serializable]
public class StorageContainer {
    protected List<StorageInfo> m_StorageInfo;
    public int maxCapacity;



	public StorageContainer() {
		m_StorageInfo = new List<StorageInfo>();
	}



    public int GetFreeSpace() {
        int usedSpace = 0;
        for(int i = 0; i < m_StorageInfo.Count; ++i) {
            usedSpace += m_StorageInfo[i].currentAmount + m_StorageInfo[i].reservedAmount;
        }
        return(maxCapacity - usedSpace);
    }



	public int GetStoredSpace() {
		int storedSpace = 0;

		for(int i = 0; i < m_StorageInfo.Count; ++i) {
			storedSpace += m_StorageInfo[i].currentAmount;
		}

		return(storedSpace);
	}



    public bool CanStore(Storage storage) {
        return(storage.quantity <= GetFreeSpace());
    }



	public int GetReservedAmount(StorageData storageData) {
		int reserved = 0;

		for(int i = 0; i < m_StorageInfo.Count; ++i) {
			if(m_StorageInfo[i].stored == storageData) {
				reserved = m_StorageInfo[i].reservedAmount;
			}
		}

		return(reserved);
	}



    public void Reserve(StorageData storageData, int amount) {
		Assert.Test(amount <= GetFreeSpace());

        int infoIndex = GetStorageIndex(storageData);
        StorageInfo info = infoIndex >= 0 ? m_StorageInfo[infoIndex] : new StorageInfo(storageData, 0, 0);

        if(infoIndex < 0) {
            infoIndex = m_StorageInfo.Count;
            m_StorageInfo.Add(info);
        }

        m_StorageInfo[infoIndex] = new StorageInfo(info.stored, info.currentAmount, info.reservedAmount + amount);
    }



    public void StockReserved(StorageData storageData, int amount) {
        int infoIndex = GetStorageIndex(storageData);
        Assert.Test(infoIndex >= 0);

        if(infoIndex >= 0) {
            StorageInfo info = m_StorageInfo[infoIndex];
            Assert.Test(amount <= info.reservedAmount);

            m_StorageInfo[infoIndex] = new StorageInfo(
                info.stored, info.currentAmount + amount, info.reservedAmount - amount
            );
        }
    }



    public int GetStorageIndex(StorageData storageData) {
        for(int i = 0; i < m_StorageInfo.Count; ++i) {
            if(m_StorageInfo[i].stored == storageData) return(i);
        }
        return(-1);
    }



	public float GetStoredPercentage() {
		return(GetStoredSpace() / (float) maxCapacity);
	}
}