﻿using UnityEngine;
using System.Collections;



public struct StorageInfo {
    readonly public StorageData stored;
    readonly public int currentAmount;
    readonly public int reservedAmount; // Someone is bringing this amount back



    public StorageInfo(StorageData stored, int currentAmount, int reservedAmount) {
        this.stored = stored;
        this.currentAmount = currentAmount;
        this.reservedAmount = reservedAmount;
    }
}
