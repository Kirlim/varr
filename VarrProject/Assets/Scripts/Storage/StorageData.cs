﻿using UnityEngine;
using System.Collections;



public class StorageData : ScriptableObject {
    public ItemTypes itemType;
    public string displayName;
    public string displayDescription;
}
