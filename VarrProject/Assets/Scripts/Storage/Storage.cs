﻿using UnityEngine;
using System.Collections;



/// <summary>
/// Represents the instance of something stored.
/// </summary>
[System.Serializable]
public struct Storage {
    public StorageData data;
    public int quantity;
}