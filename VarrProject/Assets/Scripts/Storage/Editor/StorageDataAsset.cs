﻿using UnityEngine;
using UnityEditor;
using System.Collections;



public class StorageDataAsset {
    [MenuItem("Assets/Create/Storage Data Asset")]
    public static void CreateAsset() {
        ScriptableObjectUtility.CreateAsset<StorageData>();
    }
}

