﻿using UnityEngine;
using System.Collections;



public class CameraBehavior : MonoBehaviour {
    public float height = 50.0f;
    public float sightDistance = 200;
    public float convergeFactor;
    protected float m_CurrentAltitude;

    protected Transform m_Transform;



    protected void Awake() {
        m_Transform = this.transform;
        m_CurrentAltitude = m_Transform.position.y;
    }



    protected void LateUpdate() {
        Vector3 pos = m_Transform.position;

        float highestSightAltitude = CalculateDesiredHeight();

        m_CurrentAltitude = Mathf.Lerp(
            m_CurrentAltitude, highestSightAltitude, convergeFactor * Time.deltaTime
        );

        pos.y = m_CurrentAltitude;
        m_Transform.position = pos;
    }



    protected float m_HeightCalculationAux;
    protected float CalculateDesiredHeight() {
        // We will use Bresenham's algorithm!

        // Prepare data

        // Adding 90 to the camera angle, since camera angle 0 is z positive, and
        // 'angle' 0 is x positive
        float angle = (-m_Transform.eulerAngles.y + 90.0f) * Mathf.Deg2Rad;
        Vector3 startPos = m_Transform.position;
        Vector3 endPos = startPos + new Vector3(
            Mathf.Cos(angle) * sightDistance, 0.0f, Mathf.Sin(angle) * sightDistance
        );

        IntVector2 start = Global.Map.GetPosAsTile(startPos);
        IntVector2 end = Global.Map.GetPosAsTile(endPos);

//        Debug.LogWarning(start.x + " " + start.y + " " + end.x + " " + end.y);
        m_HeightCalculationAux = float.MinValue;
        MyMath2D.BresenhamsLine(start.x, start.y, end.x, end.y, GetHighestAltitude);

        if(m_HeightCalculationAux > float.MinValue) {
            return(m_HeightCalculationAux + height);
        } else {
            return(m_CurrentAltitude);
        }
    }



    protected void GetHighestAltitude(int x, int y) {
        if(x < 0 || y < 0 || x >= Global.Map.Width || y >= Global.Map.Height) return;
        m_HeightCalculationAux = Mathf.Max(m_HeightCalculationAux, Global.Map.GetTileHeight(x, y));

//        Debug.Log("(" + x + "; " + y + ") " + Global.Map.GetTileHeight(x, y));
   }
}
