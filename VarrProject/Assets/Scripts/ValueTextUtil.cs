﻿using UnityEngine;
using System.Collections;




public class ValueTextUtil : MonoBehaviour {
	public TextMesh textMeshRef;



	public void SetText(string text) {
		if(textMeshRef != null) {
			textMeshRef.text = text;
		}
	}
}
