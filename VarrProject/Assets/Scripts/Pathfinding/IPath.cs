﻿using UnityEngine;
using System.Collections;



public interface IPath {
	IntVector2 Pop();
	IntVector2 Peek();
	bool Empty();
}
