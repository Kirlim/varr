﻿using UnityEngine;
using System.Collections;



/// <summary>
/// Simple structure that holds parameters that might change
/// pathfinding behavior.
/// </summary>
[System.Serializable]
public struct PathPolicy {
	[Immutable] public int neutralCost;
	[Immutable] public int roadCost;
	[Immutable] public int farmCost;

	public PathPolicy(int neutralCost, int roadCost, int farmCost) {
		this.neutralCost = neutralCost;
		this.roadCost = roadCost;
		this.farmCost = farmCost;
	}
}
