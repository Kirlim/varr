﻿using UnityEngine;
using System.Collections;



/// <summary>
/// Binary heap for the A* algorithm. Works with a vector of index for AStarNodes
/// </summary>
public class AStarBinaryHeap {
	private AStarNode[] m_Nodes = null;
	private int[] m_HeapData = null; //Note: "starts" on index 1, for performance.
	
	private int m_CurrentSize = 0;
	
	
	
	public void InitiateHeap(AStarNode[] nodes) {
		this.m_Nodes = nodes;
		m_HeapData = new int[nodes.Length + 1];
		
		m_CurrentSize = 0;
	}
	
	
	
	public void AddNode(int node, int parentNode, int walkCost, int heuristic) {
		++m_CurrentSize;	
		
		int TotalCost = walkCost + heuristic;
		m_Nodes[node].parent = parentNode;
		m_Nodes[node].walkCost = walkCost;
		m_Nodes[node].totalCost = TotalCost;
		m_Nodes[node].heapIndex = m_CurrentSize;
		m_HeapData[m_CurrentSize] = node;
		
		
		int iterator = m_CurrentSize, parent = 0, swap_tmp = 0;
		while(iterator != 1) {
			parent = iterator / 2;
			if(m_Nodes[m_HeapData[iterator]].totalCost <= m_Nodes[m_HeapData[parent]].totalCost) {
				m_Nodes[m_HeapData[iterator]].heapIndex = parent;
				m_Nodes[m_HeapData[parent]].heapIndex = iterator;
				swap_tmp = m_HeapData[iterator];
				m_HeapData[iterator] = m_HeapData[parent];
				m_HeapData[parent] = swap_tmp;
				
				iterator = parent;
			} else {
				return;
			}
		}
	}
	
	
	
	public int FetchTop() {
		int return_value = m_HeapData[1];
		m_Nodes[return_value].heapIndex = -1;
		m_Nodes[m_HeapData[m_CurrentSize]].heapIndex = 1;
		m_HeapData[1] = m_HeapData[m_CurrentSize];
		--m_CurrentSize;
		
		int current = 1, candidate = 1, swap_tmp = 0;
		while(true) { //Swap with lowest child
			candidate = current;
			if((current * 2) + 1 <= m_CurrentSize) {
				if(m_Nodes[m_HeapData[current]].totalCost > m_Nodes[m_HeapData[current * 2]].totalCost) candidate = current * 2;
				if(m_Nodes[m_HeapData[candidate]].totalCost > m_Nodes[m_HeapData[(current * 2) + 1]].totalCost) candidate = (current * 2) + 1;
			} else if(current * 2 <= m_CurrentSize) {
				if(m_Nodes[m_HeapData[current]].totalCost > m_Nodes[m_HeapData[current * 2]].totalCost) candidate = current * 2;
			}
			
			if(current != candidate) {
				m_Nodes[m_HeapData[current]].heapIndex = candidate;
				m_Nodes[m_HeapData[candidate]].heapIndex = current;
				swap_tmp = m_HeapData[current];
				m_HeapData[current] = m_HeapData[candidate];
				m_HeapData[candidate] = swap_tmp;
				
				current = candidate;
			} else {
				return(return_value);
			}
		}
	}
	
	
	
	public int GetSize() {
		return(m_CurrentSize);
	}
	
	
	
	/// <summary>
	/// Updates the node if needed. The node should never move down, only up. Using values to move
	/// the node down is not in the semantic of usage of the A* algorithm and thus this method
	/// aborts update if a node should move down in the heap.
	/// </summary>
	public void UpdateIfNeeded(int index, int newParent, int walkCost, int heuristic) {
		if(walkCost + heuristic >= m_Nodes[m_HeapData[index]].totalCost) return;
		
		m_Nodes[m_HeapData[index]].walkCost = walkCost;
		m_Nodes[m_HeapData[index]].totalCost = walkCost + heuristic;
		m_Nodes[m_HeapData[index]].parent = newParent;
		
		int iterator = index, parent = 0, swap_tmp = 0;
		while(iterator != 1) {
			parent = iterator / 2;
			if(m_Nodes[m_HeapData[iterator]].totalCost <= m_Nodes[m_HeapData[parent]].totalCost) {
				m_Nodes[m_HeapData[iterator]].heapIndex = parent;
				m_Nodes[m_HeapData[parent]].heapIndex = iterator;
				swap_tmp = m_HeapData[iterator];
				m_HeapData[iterator] = m_HeapData[parent];
				m_HeapData[parent] = swap_tmp;
				
				iterator = parent;
			} else {
				return;
			}
		}
	}
	
	
	
	public void ClearHeap() {
		m_CurrentSize = 0;
	}
}