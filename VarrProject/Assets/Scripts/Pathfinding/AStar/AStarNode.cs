﻿using UnityEngine;
using System.Collections;



/// <summary>
/// Node to be used with A* algorithm.
/// </summary>
public struct AStarNode {
	public int parent;
	public int heapIndex; // if != -1, is in the open list
	public int walkCost;
	public int totalCost; //Sacrificing memory for slight performance improvement
	public int status; //Used to know if it is in the closed list or not
	
	public AStarNode(int parent, int heapIndex, int walkCost, int heuristic, int status) {
		this.parent = parent;
		this.heapIndex = heapIndex;
		this.walkCost = walkCost;
		this.totalCost = walkCost + heuristic;
		this.status = status;
	}
}