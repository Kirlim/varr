﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class AStarPath : IPath {
	protected Stack<IntVector2> m_Path;



	public AStarPath() {
		m_Path = new Stack<IntVector2>();
	}



	public IntVector2 Pop() {
		return(m_Path.Pop());
	}



	public IntVector2 Peek() {
		return(m_Path.Peek());
	}



	public bool Empty() {
		return(m_Path.Count == 0);
	}



	public void Push(IntVector2 pos) {
		m_Path.Push(pos);
	}
}
