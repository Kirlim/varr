﻿using UnityEngine;
using System.Collections;



public class AStar : IPathfinder {
	//Class variables
	private int m_MapWidth = 0, m_MapHeight = 0;
	private AStarNode[] m_MapNodes = null;
	private int m_CurrentClosedStatus = 0; //Used to know if a node is in the closed list or not
	
	//Navigability matrix are packed ints with wasted space. They use constant
	//int masks for speed. Starts from bottom left, ends at top right. Flag true = walkable
	private byte[] m_NavigabilityMatrix;
	private const byte Self = 1, NavNorth = 2, NavSouth = 4, NavEast = 8, NavWest = 16;
	
	private int m_DestX = 0, m_DestY = 0; // Start and end of a path, cached
	private PathPolicy m_PathPolicy; // Path policy cached

	private AStarBinaryHeap m_OpenList = new AStarBinaryHeap();

	private Map m_Map;

	
	
	public void Initialize(Map map) {
		m_Map = map;

		this.m_MapWidth = map.Width;
		this.m_MapHeight = map.Height;

		GenerateNodesVector();
		GenerateNavigabilityMatrix();

		m_OpenList.InitiateHeap(m_MapNodes);
	}
	
	
	
	private void GenerateNodesVector() {
		m_MapNodes = new AStarNode[m_MapWidth * m_MapHeight];
		for(int i = 0; i < m_MapNodes.Length; ++i) {
			m_MapNodes[i].heapIndex = -1;
			m_MapNodes[i].walkCost = 0;
			m_MapNodes[i].totalCost = 0;
			m_MapNodes[i].parent = 0;
		}
	}
	
	
	
	private void GenerateNavigabilityMatrix() {
		m_NavigabilityMatrix = new byte[m_MapWidth * m_MapHeight];
		for(int i = 0; i < m_NavigabilityMatrix.Length; ++i) {
			m_NavigabilityMatrix[i] = 0;
		}
		
		
		for(int x = 0; x < m_MapWidth; ++x) {
			for(int y = 0; y < m_MapHeight; ++y) {
				int nav_node = (y * m_MapWidth) + x;
				
				if(IsPositionFree(x-1, y)) m_NavigabilityMatrix[nav_node] += NavWest;
				if(IsPositionFree(x, y-1)) m_NavigabilityMatrix[nav_node] += NavSouth;
				if(IsPositionFree(x+1, y)) m_NavigabilityMatrix[nav_node] += NavEast;
				if(IsPositionFree(x, y+1)) m_NavigabilityMatrix[nav_node] += NavNorth;
			}
		}
	}
	
	
	
	private bool IsPositionFree(int x, int y) {
		if(x < 0 || y < 0 || x >= m_MapWidth || y >= m_MapHeight) return(false);
		return(true);
	}
	
	

	public IPath GetPath(IntVector2 origin, IntVector2 destination, PathPolicy pathPolicy) {
		//Clear data for new search
		m_OpenList.ClearHeap();
		m_CurrentClosedStatus += 2;
		
		//Early return, or abortions
		if(origin.x == destination.x && origin.y == destination.y) return(new AStarPath());
		if(destination.x < 0   || destination.x >= m_MapWidth  ||
		   destination.y < 0   || destination.y >= m_MapHeight   ||
		   origin.x < 0 || origin.x > m_MapWidth || origin.x < 0 || origin.y >= m_MapHeight
		){
			return(null);
		}

		m_PathPolicy = pathPolicy;
		this.m_DestX = destination.x;
		this.m_DestY = destination.y;
		
		
		m_OpenList.AddNode(
			(origin.y * m_MapWidth) + origin.x, -1, 0, GetHeuristic((origin.y * m_MapWidth) + origin.x)
		); //First node into list

		int dest = (destination.y * m_MapWidth) + destination.x;
		
		
		//Pathfinding loop
		int currentNode = 0, currentCost = 0;
		bool destinationFound = false;
		

		
		while(m_OpenList.GetSize() > 0) {
			currentNode = m_OpenList.FetchTop();
			currentCost = m_MapNodes[currentNode].walkCost;
			m_MapNodes[currentNode].status = m_CurrentClosedStatus;
			
			if(currentNode == dest) {
				destinationFound = true;
				break;
			}

			// Get cost of curret type
			int thisCost = m_PathPolicy.neutralCost;
			int x = currentNode % m_MapWidth;
			int y = currentNode / m_MapWidth;
			if(m_Map.IsRoad(x, y)) thisCost = m_PathPolicy.roadCost;
			if(m_Map.IsFarm(x, y)) thisCost = m_PathPolicy.farmCost;

			currentCost += thisCost;

			// TODO : FIX "WARP" FROM NODE COORDINATES

			//Add neighbours
			if((m_NavigabilityMatrix[currentNode] & NavNorth) != 0) {
				AddNeighbour(currentNode + m_MapWidth, currentNode, currentCost);
			}
			if((m_NavigabilityMatrix[currentNode] & NavSouth) != 0) {
				AddNeighbour(currentNode - m_MapWidth, currentNode, currentCost);
			}
			if((m_NavigabilityMatrix[currentNode] & NavEast) != 0) {
				AddNeighbour(currentNode + 1, currentNode, currentCost);
			}
			if((m_NavigabilityMatrix[currentNode] & NavWest) != 0) {
				AddNeighbour(currentNode - 1, currentNode, currentCost);
			}
		}
		
		
		if(destinationFound) {
			AStarPath path = new AStarPath();

			int startNode = (origin.y * m_MapWidth) + origin.x;
			int readedNode = dest;
			
			bool startInserted = false;
			do {
				int x = readedNode % m_MapWidth;
				int y = readedNode / m_MapWidth;
				
				path.Push(new IntVector2(x, y));
				
				if(readedNode == startNode) {
					startInserted = true;
				} else {
					readedNode = m_MapNodes[readedNode].parent;
				}
			} while(startInserted == false);
			
			return(path);
		} else {
			return(null);
		}
	}
	
	
	
	private void AddNeighbour(int node, int parent, int walkCost) {
		UnityEngine.Profiling.Profiler.BeginSample("A* AddNeighbour Policy");

		bool allowed = true; // TODO : INJECT BLOCKED TILES
		int x = node % m_MapWidth;
		int y = node / m_MapWidth;

		// TODO : BETTER LOGIC OF SOLID BLOCKS FORM WALKABILITY
		if(!m_Map.IsFree(x, y) && !m_Map.IsFarm(x, y)) allowed = false;

		UnityEngine.Profiling.Profiler.EndSample();

		if(!allowed) return;


		UnityEngine.Profiling.Profiler.BeginSample("A* AddNeighbour Logic");

		if(m_MapNodes[node].status < m_CurrentClosedStatus) {
			if(m_MapNodes[node].status < m_CurrentClosedStatus - 1) {
				//Not on open list
				m_MapNodes[node].status = m_CurrentClosedStatus - 1;
				m_OpenList.AddNode(node, parent, walkCost, GetHeuristic(node));
			} else {
				//On open list
				// Debug.Log("Update if needed");
				m_OpenList.UpdateIfNeeded(m_MapNodes[node].heapIndex, parent, walkCost, GetHeuristic(node));
			}
		}// else {
//			Debug.LogWarning("Attempt to add closed node [" + (node % m_MapWidth) + " - " + (node / m_MapWidth) + "]");
//		}

		UnityEngine.Profiling.Profiler.EndSample();
	}
	
	
	
	private int GetHeuristic(int node) {
		return(
			(Mathf.Abs(m_DestX - (node % m_MapWidth)) +
			 Mathf.Abs(m_DestY - (node / m_MapWidth))) * 10
		);
	}
}
