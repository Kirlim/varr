﻿using UnityEngine;
using System.Collections;



public interface IPathfinder {
	void Initialize(Map map);
	IPath GetPath(IntVector2 origin, IntVector2 destination, PathPolicy pathPolicy);
}
