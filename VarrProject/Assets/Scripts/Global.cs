﻿using UnityEngine;
using System.Collections;




public class Global : MonoBehaviour {
	public Map mapRef;
	public FarmsManager farmsManagerRef;
    public GranariesManager granariesManagerRef;
	public SimpleFightersGroupsManagers fightersGroupsManagerRef;
	public DamageIndicatorsManager damageIndicatorsManagerRef;
    public MapObjectsManager mapObjectsManagerRef;

	static public IPathfinder PathFinder { get; private set; }
	static public Map Map { get; private set; }
	static public IFarmsManager FarmsManager { get; private set; }
    static public IGranariesManager GranariesManager { get; private set; }
	static public IFightersGroupsManager FightersGroupsManager { get; private set; }
	static public DamageIndicatorsManager DamageIndicators { get; private set; }
    static public MapObjectsManager MapObjectsManager { get; private set; }



	private void Awake() {
		PathFinder = new AStar();

		Map = mapRef;
		FarmsManager = farmsManagerRef;
        GranariesManager = granariesManagerRef;
		FightersGroupsManager = fightersGroupsManagerRef;
		DamageIndicators = damageIndicatorsManagerRef;
        MapObjectsManager = mapObjectsManagerRef;
	}



	private void Start() {
		PathFinder.Initialize(mapRef);
	}
}
