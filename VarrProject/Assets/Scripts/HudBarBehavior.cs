﻿using UnityEngine;
using System.Collections;



/// <summary>
/// Scales a game object in the x axis
/// </summary>
public class HudBarBehavior : MonoBehaviour {
	public Transform foregroundBar;
	public float showPct;
	public float fullScale;



	protected void Start() {
		SetPercentage(showPct);
	}



	public void SetPercentage(float showPct) {
		this.showPct = Mathf.Clamp(showPct, 0.0f, 1.0f);
		Refresh();
	}



	[ContextMenu("Refresh")]
	protected void Refresh() {
		float currentScale = showPct * fullScale;

		Vector3 scale = foregroundBar.localScale;
		scale.x = currentScale;
		foregroundBar.localScale = scale;
	}
}
