﻿using UnityEngine;
using System.Collections;



/// <summary>
/// Randomly wander around from time to time
/// </summary>
public class MovRandomWanderBehavior : MonoBehaviour {
	public float minWaitBeforeMov, maxWaitBeforeMove;
	protected float m_CurrentWaitRemaining;
	public MovementBehavior movement;
	public FightersGroupBehavior fighterGroup;

	public int maxMovDist;

	protected Transform m_Transform;



	protected void Awake() {
		m_Transform = this.transform;
	}



	protected void Update() {
		if(movement.State != MovementStates.Moving && m_CurrentWaitRemaining <= 0.0f) {
			IntVector2 newPos = IntVector2.zero;
			if((fighterGroup == null || !fighterGroup.Engaged)) {
				if(GetNewPosition(out newPos)) {
					movement.MoveTo(newPos);
					m_CurrentWaitRemaining = Random.Range(minWaitBeforeMov, maxWaitBeforeMove);
				}
			}
		} else {
			m_CurrentWaitRemaining -= Time.deltaTime;
		}
	}



	protected bool GetNewPosition(out IntVector2 desiredPos) {
		bool foundPos = false;
		desiredPos = IntVector2.zero;

		IntVector2 pos = Global.Map.GetPosAsTile(m_Transform.position);
		pos.x += Random.Range(-maxMovDist, maxMovDist + 1);
		pos.y += Random.Range(-maxMovDist, maxMovDist + 1);

		pos.x = Mathf.Clamp(pos.x, 0, Global.Map.Width - 1);
		pos.y = Mathf.Clamp(pos.y, 0, Global.Map.Height - 1);

		if(Global.Map.IsFree(pos.x, pos.y)) {
			foundPos = true;
			desiredPos = pos;
		}

		return(foundPos);
	}
}
