﻿using UnityEngine;
using System.Collections;



/// <summary>
/// Attempts to defend an area.
/// </summary>
public class GuardBehavior : WorkerBehavior {
	public int watchMaxHouseDist;
	public FightersGroupBehavior fighterGroup;

	public enum States { Idle, Watching, GoingHome }
	public States CurrentState { get; protected set; }



	protected override void Awake() {
		base.Awake();
		CurrentState = States.Idle;
	}



	protected void Update() {
		switch(CurrentState) {
		case States.Idle:      ExecuteIdleState();      break;
		case States.Watching:  ExecuteWatchingState();  break;
		case States.GoingHome: ExecuteGoingHomeState(); break;
		}
	}



	protected void ExecuteIdleState() {
		CurrentState = States.Watching;
	}



	protected void ExecuteWatchingState() {
		if(
			!fighterGroup.Engaged &&
			(m_Movement.State == MovementStates.Arrived || m_Movement.State == MovementStates.Idle)
		) {
			IntVector2 newDest = GetNewRandomWatchPosition();
			m_Movement.MoveTo(newDest);
		}
	}



	protected void ExecuteGoingHomeState() {

	}



	protected IntVector2 GetNewRandomWatchPosition() {
		IntVector2 pos = Global.Map.GetPosAsTile(m_Home.transform.position); // TODO : BETTER WAY TO DO THIS

		pos.x += Random.Range(-watchMaxHouseDist, watchMaxHouseDist + 1);
		pos.y += Random.Range(-watchMaxHouseDist, watchMaxHouseDist + 1);
		pos.x = Mathf.Clamp(pos.x, 0, Global.Map.Width - 1);
		pos.y = Mathf.Clamp(pos.y, 0, Global.Map.Height - 1);

		return(pos);
	}
}
