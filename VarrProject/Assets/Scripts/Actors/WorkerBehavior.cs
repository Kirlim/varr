﻿using UnityEngine;
using System.Collections;




[AddComponentMenu("Scripts/Actors/Worker Behavior")]
[RequireComponent(typeof(MovementBehavior))]
public class WorkerBehavior : MonoBehaviour {
	protected WorkersHouseBehavior m_Home;
	public float Stamina { get; protected set; }

	protected MovementBehavior m_Movement;



	protected virtual void Awake() {
		m_Movement = this.GetComponent<MovementBehavior>();
	}



	public void SetHome(WorkersHouseBehavior home) {
		m_Home = home;
	}
}
