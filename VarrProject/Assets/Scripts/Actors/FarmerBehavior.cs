﻿using UnityEngine;
using System.Collections;



/// <summary>
/// Farmers attempts to take care of farms. Duh!
/// </summary>
[AddComponentMenu("Scripts/Actors/Farmer Behavior")]
public class FarmerBehavior : WorkerBehavior {
	protected FarmWorkData m_WorkData;

    public enum States { Idle, Sowing, Treating, Reaping, ChoosingStorage, Storing}
    public States CurrentState { get; protected set; }



	protected override void Awake() {
		base.Awake();
        CurrentState = States.Idle;
	}



	protected void Update() {
        switch(CurrentState) {
        case States.Idle:            ExecuteIdleState();            break;
        case States.Sowing:          ExecuteSowingState();          break;
        case States.Treating:        ExecuteTreatingState();        break;
        case States.Reaping:         ExecuteReapingState();         break;
        case States.ChoosingStorage: ExecuteChoosingStorageState(); break;
        case States.Storing:         ExecuteStoringState();        break;
        }
	}



    protected void ExecuteIdleState() {
		if(m_WorkData == FarmWorkData.Empty) {
			IntVector2 tPos = Global.Map.GetPosAsTile(this.transform.position);
			IntVector2 hPos = Global.Map.GetPosAsTile(m_Home.transform.position); // TODO : BETTER WAY TO DO THIS
			
			m_WorkData = Global.FarmsManager.GetWork(this, tPos, hPos);
		} else {
            IntVector2 farmTilePos = m_WorkData.farm.GetTilePos();
            m_Movement.MoveTo(farmTilePos);

            if(m_Movement.State != MovementStates.Idle) {
                switch(m_WorkData.farm.GetWork()) {
                case FarmWorks.Sow:   GoToState(States.Sowing);   break;
                case FarmWorks.Treat: GoToState(States.Treating); break;
                case FarmWorks.Reap:  GoToState(States.Reaping);  break;
                }
            }
        }
    }



    protected void ExecuteSowingState() {

    }



    protected void ExecuteTreatingState() {

    }



    protected void ExecuteReapingState() {
        if(m_Movement.State == MovementStates.Arrived) {
            m_WorkData.farm.Reap();
            m_Movement.FinishMovement();

            GoToState(States.ChoosingStorage);
        }
    }



    protected void ExecuteChoosingStorageState() {
		Assert.Test(m_WorkData.granary != null);

	    IntVector2 granaryTilePos = m_WorkData.granary.GetEntranceTilePos();
	    m_Movement.MoveTo(granaryTilePos);

	    if(m_Movement.State != MovementStates.Idle) {
	        GoToState(States.Storing);
	    }
    }



    protected void ExecuteStoringState() {
        if(m_Movement.State == MovementStates.Arrived) {
			m_WorkData.granary.StoreReserved(m_WorkData.farm.Crop);
            m_Movement.FinishMovement();

			m_WorkData = FarmWorkData.Empty;

            GoToState(States.Idle);
        }
    }



    protected void GoToState(States nextState) {
        CurrentState = nextState;
    }
}
