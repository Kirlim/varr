﻿using UnityEngine;
using System.Collections;



/// <summary>
/// Will simply attempt to copy the camera rotation
/// </summary>
public class SimpleBillboardBehavior : MonoBehaviour {
	[Tooltip("Camera to use as a base. If null, will use the main camera")]
	public Camera cam;
	public bool copyXRot;
	public bool copyYRot;
	public bool copyZRot;

	protected Transform m_Trasform;



	protected void Awake() {
		m_Trasform = this.transform;
	}



	protected void Start() {
		Refresh();
	}



	protected void Update() {
		Refresh();
	}



	protected void Refresh() {
		Camera c = cam;
		if(c == null) c = Camera.main;

		Vector3 camRot = c.transform.eulerAngles;
		Vector3 myRot = m_Trasform.eulerAngles;
		if(copyXRot) myRot.x = camRot.x;
		if(copyYRot) myRot.y = camRot.y;
		if(copyZRot) myRot.z = camRot.z;
		m_Trasform.eulerAngles = myRot;
	}
}
