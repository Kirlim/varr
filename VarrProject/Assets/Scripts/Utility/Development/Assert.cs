using UnityEngine;
using System.Collections;



// Snippet modified from Ntero's code posted in the unity forums.
public class Assert {
	public const string AssertionFailed = "Assertion failed.";
	public const string AssertionThrow = "Assertion throw.";

	[System.Diagnostics.Conditional("DEBUG_GAME")]
	public static void Test(bool comparison) {
		Test(comparison, AssertionFailed);
	}
	
	[System.Diagnostics.Conditional("DEBUG_GAME")]
	public static void Test(bool comparison, string message) {
		Test(comparison, message, null);
	}
	
	[System.Diagnostics.Conditional("DEBUG_GAME")]
	public static void Test(bool comparison, string message, GameObject gameObject) {
		if(!comparison) {
			Debug.LogWarning(message, gameObject);
			Debug.Break();
		}
	}
	
	
	
	[System.Diagnostics.Conditional("DEBUG_GAME")]
	public static void Throw() {
		Debug.LogWarning(AssertionThrow);
	}
	
	[System.Diagnostics.Conditional("DEBUG_GAME")]
	public static void Throw(string message) {
		Debug.LogWarning(message);
	}
}
