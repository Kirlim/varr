﻿using UnityEngine;
using System.Collections;



/// <summary>
/// Small set of methods to help with 3D stuff
/// </summary>
public class MyMath3D {
	static public Vector3 GetNormalizedNormal(Vector3 v, Vector3 l, Vector3 r) {
		Vector3 vl = l - v;
		Vector3 vr = r - v;
		return(Vector3.Cross(vl, vr).normalized);
	}



    /// <summary>
    /// Raycast against triangle. Ray is assumed to be normalized!
    /// This method has not been carefully tested!
    /// </summary>
    static public bool RayTriIntersectTrumbore(
        Ray ray, Vector3 a, Vector3 b, Vector3 c, float EPILSON, out float distance
    ) {
        // Moller Trumbore intersection algorithm
        distance = float.MaxValue;

        // Find two edges sharing vertex a
        Vector3 edge1 = b - a;
        Vector3 edge2 = c - a;

        // Begin calculating determinant. Can also beused to calculate U parameter
        Vector3 pVec = Vector3.Cross(ray.direction, edge2);

        // If determinant is near zero, the ray lies in the plane of the triangle.
        float det = Vector3.Dot(edge1, pVec);

        // Non culling branch
        if(det > -EPILSON && det < EPILSON) return(false);

        float invDet = 1.0f / det;

        // Calculate the distance from 'a' to ray origin
        Vector3 tVec = ray.origin - a;

        // Calculate 'u' parameter and test bounds
        float u = Vector3.Dot(pVec, tVec) * invDet;
        if(u < 0.0f - EPILSON || u > 1.0f + EPILSON) return(false);

        // Prepare to test 'v' parameter
        Vector3 qVec = Vector3.Cross(tVec, edge1);

        // Calculate 'v' parameter and test bounds
        float v = Vector3.Dot(ray.direction, qVec) * invDet;
        if(v < 0.0f - EPILSON || (u + v) > 1.0f + EPILSON) return(false);

        // Calculate 't' (distance), ray intersects triangle
        distance = Vector3.Dot(edge2, qVec) * invDet;

        return(true);

        

//        distance = float.MaxValue;
//
//        Vector3 edge1 = b - a;
//        Vector3 edge2 = c - a;
//        
//        // Determinant and used to calculate u parameter
//        Vector3 p = Vector3.Cross(ray.direction, edge2);
//        
//        // If determinant is near zeor, ray is in the plane of the triangle
//        float det = Vector3.Dot(edge1, p);
//        
//        if(det > -EPILSON && det < EPILSON) return(false);
//        float invDet = 1.0f / det;
//        
//        // Distance from v1 to ray origin
//        Vector3 t = ray.origin - a;
//        
//        // Calculate u parameter
//        float u = Vector3.Dot(t, p) * invDet;
//        if( u < 0.0f || u > 1.0f) return(false); // Intersection outside the triangle
//        
//        // Calculate v parameten
//        Vector3 q = Vector3.Cross(t, edge1);
//        float v = Vector3.Dot(ray.direction, q) * invDet;
//        if(v < 0.0f || u + v > 1.0f) return(false); // Intersection outside the triangle
//        
//        float d = Vector3.Dot(edge2, q) * invDet;
//        
//        if(d > EPILSON) {
//            // Ray intersetction
//            distance = d;
//            return(true);
//        } else {
//            return(false);
//        }
    }
}
