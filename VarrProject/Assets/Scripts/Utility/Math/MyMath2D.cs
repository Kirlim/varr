﻿using UnityEngine;
using System.Collections;



/// <summary>
/// Collection of math methods for some generic 2D math.
/// </summary>
public class MyMath2D {
	static public float tolerance = 0.00001f;
	static public bool WithinTolerance(float a, float b) {
		return(Mathf.Abs(a - b) < tolerance * Mathf.Max(1.0f, Mathf.Max(a, b)));
	}
	static public bool WithinTolerance(float value, float a, float b) {
		return(Mathf.Abs(value) < tolerance * Mathf.Max(1.0f, Mathf.Max(a, b)));
	}



	/// <summary>
	/// Return the point of a segment that is closest to another point.
	/// </summary>
	static public Vector2 ClosestPointToSegment(Vector2 point, Vector2 segmentP1, Vector2 segmentP2) {
		Vector2 segment = segmentP2 - segmentP1;
		
		Vector2 pointToSegmentP1 = point - segmentP1;
		float dotPointToP1 = Vector2.Dot(pointToSegmentP1, segment);
		if(dotPointToP1 <= 0) return(segmentP1);
		
		Vector2 pointToSegmentP2 = segmentP2 - point;
		float dotPointToP2 = Vector2.Dot(pointToSegmentP2, segment);
		if(dotPointToP2 <= 0) return(segmentP2);
		
		Vector2 finalPoint = Vector2.zero;
		finalPoint.x = segmentP1.x + ((segment.x * dotPointToP1) / (dotPointToP1 + dotPointToP2));
		finalPoint.y = segmentP1.y + ((segment.y * dotPointToP1) / (dotPointToP1 + dotPointToP2));
		return(finalPoint);
	}



	/// <summary>
	/// Returns the euclidian distance of a point to a line segment.
	/// </summary>
	static public float PointToSegmentDistance(Vector2 point, Vector2 segmentP1, Vector2 segmentP2) {
		Vector2 closest = ClosestPointToSegment(point, segmentP1, segmentP2);
		closest -= point; // To calculate distance
		float sqrDistance = closest.x * closest.x + closest.y * closest.y;
		return(Mathf.Sqrt(sqrDistance));
	}



	/// <summary>
	/// Returns the squared euclidian distance of a point to a line segment.
	/// A little bit faster, and more resistant to epilson comparisons if distance
	/// is near zero.
	/// </summary>
	static public float SqrPointToSegmentDistance(Vector2 point, Vector2 segmentP1, Vector2 segmentP2) {
		Vector2 closest = ClosestPointToSegment(point, segmentP1, segmentP2);
		closest -= point; // To calculate distance
		return(closest.x * closest.x + closest.y * closest.y);
	}
	
	
	
	/// <summary>
	/// Returns the side that a point is from a line defined by
	/// a segment. 1 is left, -1 is right. 0 is on line.
	/// </summary>
	static public int PointSideFromSegment(Vector2 point, Vector2 segmentP1, Vector2 segmentP2) {
		float result = (segmentP2.x - segmentP1.x) * (point.y - segmentP1.y) -
						(segmentP2.y - segmentP1.y) * (point.x - segmentP1.x);
		
		if(result < 0.0f) return(-1);
		else if(result > 0.0f) return(1);
		else return(0);
	}



	/// <summary>
	/// Simple 2D cross product that can be used to check for vectors orientation
	/// to another (shortest angle based). Positive is counter clockwise, negative
	/// is clockwise.
	/// </summary>
	static public float CrossProduct2D(Vector2 u, Vector2 v) {
		return(u.x * v.y - u.y * v.x);
	}



	/// <summary>
	/// Returns if a point is inside a triangle with given vertices. Vertices must be passed
	/// in a counter clockwise order
	/// </summary>
	static public bool PointInTriangle(Vector2 point, Vector2 tVertA, Vector2 tVertB, Vector2 tVertC) {
		return(
			PointSideFromSegment(point, tVertA, tVertB) >= 0 &&
			PointSideFromSegment(point, tVertB, tVertC) >= 0 &&
			PointSideFromSegment(point, tVertC, tVertA) >= 0
		);
	}



	static public Vector2 TransformPoint(
		Vector2 point, Vector2 pivot, Vector2 scale, float radianRotation
	) {
		return(MyMath2D.TransformPoint(
			point, pivot, scale, Mathf.Sin(radianRotation), Mathf.Cos(radianRotation)
		));
	}



	static public Vector2 TransformPoint(
		Vector2 point, Vector2 pivot, Vector2 scale, float rotationSine, float rotationCosine
	) {
		Vector2 result = point - pivot;
		result = new Vector2(
			(result.x * rotationCosine * scale.x) - (result.y * rotationSine * scale.y) + pivot.x,
			(result.x * rotationSine * scale.x) + (result.y * rotationCosine * scale.y) + pivot.y
		);
		return(result);
	}



	static public bool SegmentsIntersect(
		Vector2 segment1Start, Vector2 segment1End, Vector2 segment2Start, Vector2 segment2End
	) {
		float temp1 = (segment2End.y - segment2Start.y) * (segment1Start.x - segment1End.x);
		float temp2 = (segment1Start.y - segment1End.y) * (segment2End.x - segment2Start.x);
		float d = temp1 - temp2;
		if(WithinTolerance(d, temp1, temp2)) return(false); // Parallel
		
		float t = ((segment2End.y - segment2Start.y) * (segment2End.x - segment1End.x) - 
		           (segment2End.x - segment2Start.x) * (segment2End.y - segment1End.y)) / d;
		if(t < 0.0f  || t > 1.0f) return(false); // outside [seg1start, seg1end]
		
		float s = ((segment2End.y - segment1End.y) * (segment1Start.x - segment1End.x) -
		           (segment1Start.y - segment1End.y) * (segment2End.x - segment1End.x)) / d;
		
		if(s < 0.0f || s > 1.0f) return(false); // outside [seg2start, seg2end]

		return(true);
	}



	static public bool SegmentsIntersect(
		Vector2 segment1Start, Vector2 segment1End, Vector2 segment2Start, Vector2 segment2End,
		ref Vector2 point
	) {
		float temp1 = (segment2End.y - segment2Start.y) * (segment1Start.x - segment1End.x);
		float temp2 = (segment1Start.y - segment1End.y) * (segment2End.x - segment2Start.x);
		float d = temp1 - temp2;
		if(WithinTolerance(d, temp1, temp2)) return(false); // Parallel
		
		float t = ((segment2End.y - segment2Start.y) * (segment2End.x - segment1End.x) - 
				   (segment2End.x - segment2Start.x) * (segment2End.y - segment1End.y)) / d;
		if(t < 0.0f  || t > 1.0f) return(false); // outside [seg1start, seg1end]
		
		float s = ((segment2End.y - segment1End.y) * (segment1Start.x - segment1End.x) -
		           (segment1Start.y - segment1End.y) * (segment2End.x - segment1End.x)) / d;
		
		if(s < 0.0f || s > 1.0f) return(false); // outside [seg2start, seg2end]

		point = Vector2.zero;
		point.x = t * segment1Start.x + (1 - t) * segment1End.x;
		point.y = t * segment1Start.y + (1 - t) * segment1End.y;
		
		return(true);
	}



	/// <summary>
	/// Checks if a point is inside a circle defined by a triangle, with some kind of 
	/// black magic (with determinants). The triangle points need to be in counter clockwise order.
	/// According to wikipedia:
	///  |(Ax - Dx)    (Ay - Dy)    ((Ax^2 - Dx^2) + (Ay^2 - Dy^2))|
	///  |(Bx - Dx)    (By - Dy)    ((Bx^2 - Dx^2) + (By^2 - Dy^2))|   > 0
	///  |(Cx - Dx)    (Cy - Dy)    ((Cx^2 - Dx^2) + (Cy^2 - Dy^2))|
	///  The determinant is positive if and only if point D is inside the circumcircle
	/// </summary>
	static public bool InTriangleCircle(
		Vector2 triVertexA, Vector2 triVertexB, Vector2 triVertexC, Vector2 point
	) {
		float m11 = triVertexA.x - point.x;
		float m12 = triVertexA.y - point.y;
		float m13 = (
			(triVertexA.x * triVertexA.x - point.x * point.x) +
			(triVertexA.y * triVertexA.y - point.y * point.y)
		);

		float m21 = triVertexB.x - point.x;
		float m22 = triVertexB.y - point.y;
		float m23 = (
			(triVertexB.x * triVertexB.x - point.x * point.x) +
			(triVertexB.y * triVertexB.y - point.y * point.y)
		);

		float m31 = triVertexC.x - point.x;
		float m32 = triVertexC.y - point.y;
		float m33 = (
			(triVertexC.x * triVertexC.x - point.x * point.x) +
			(triVertexC.y * triVertexC.y - point.y * point.y)
		);

		float det = (
			(m11 * m22 * m33) + (m12 * m23 * m31) + (m13 * m21 * m32) -
			(m13 * m22 * m31) - (m11 * m23 * m32) - (m12 * m21 * m33)
		);

		// TODO : IMPROVE TEST TO USE RELATIVE TOLERANCE
		return(det > MyMath2D.tolerance);
	}



	static public Vector2 PointToCircleTangent(
		Vector2 point, Vector2 circleCenter, float radius, int sign
		) {
		// Using rectangle triangle relationships
		Vector2 a = circleCenter - point;
		
		if(a.sqrMagnitude < radius * radius) {
			// Abnormal situation where point is inside the circle.
			// The tangent does not exist.
			return(circleCenter);
		}
		
		float aLength = a.magnitude;
		float bLength = Mathf.Sqrt(Mathf.Max(a.sqrMagnitude - (radius * radius), 0.0f));
		float n = radius * radius / aLength; // Radius is c
		float h = bLength * radius / aLength;
		
		// And now, the magic
		Vector2 finalPoint = new Vector2(-a.y, a.x); // Perpendicular
		finalPoint = finalPoint.normalized * h * sign; // Move towards the tangent point (away from a)
		finalPoint += circleCenter; // Go to the circle
		finalPoint -= a.normalized * n; // Arrive at the point of the tangent
		
		return(finalPoint);
	}
	
	
	
	static public Vector2 PointToCircleTangentContactPointVector(
		Vector2 point, Vector2 circleCenter, float radius, int sign
		) {
		// Using rectangle triangle relationships
		Vector2 a = circleCenter - point;
		
		if(a.sqrMagnitude < radius * radius) {
			// Abnormal situation where point is inside the circle.
			// The tangent does not exist.
			return(circleCenter);
		}
		
		float aLength = a.magnitude;
		float bLength = Mathf.Sqrt(Mathf.Max(a.sqrMagnitude - (radius * radius), 0.0f));
		float n = radius * radius / aLength; // Radius is c
		float h = bLength * radius / aLength;
		
		// And now, the magic
		Vector2 contactPoint = new Vector2(-a.y, a.x); // Perpendicular
		contactPoint = contactPoint.normalized * h * sign; // Move towards the tangent point (away from a)
		contactPoint -= a.normalized * n; // Arrive at the point of the tangent
		
		return(contactPoint);
	}
	
	
	
	static public void CirclesOuterTangent(
		Vector2 circleACenter, Vector2 circleBCenter, float radius, int sign,
		out Vector2 point1, out Vector2 point2
		) {
		// Simple case: just need to offset the circle position by
		// 'radius' distance, in a angle of 90 degrees to the segment
		// defined by the two centers.
		Vector2 line = circleBCenter - circleACenter;
		line = new Vector2(-line.y, line.x);
		line = line.normalized * radius * sign;
		
		point1 = circleACenter + line;
		point2 = circleBCenter + line;
	}
	
	
	
	static public void CirclesInnerTangent(
		Vector2 circleACenter, Vector2 circleBCenter, float radius, int sign,
		out Vector2 point1, out Vector2 point2
		) {
		// I've decided to use the point to circle tangent for this. The point
		// must be in the middle of the segment defined by both circles. Sign needs
		// to be inverted, since we "crossed" it.
		Vector2 position = circleBCenter - circleACenter;
		position = position * 0.5f;
		position += circleACenter;
		
		Vector2 guide = PointToCircleTangentContactPointVector(position, circleBCenter, radius, sign);
		point1 = circleACenter - guide;
		point2 = circleBCenter + guide;
	}



    public delegate void BresenhamsCallback(int x, int y);
    static public void BresenhamsLine(
        int x1, int y1, int x2, int y2, BresenhamsCallback callback
    ) {
        // Bresenham's line algorithm.
        bool steep = (Mathf.Abs(y2 - y1) > Mathf.Abs(x2 - x1));

        if(steep) {
            int tmp = x1; x1 = y1; y1 = tmp;
            tmp     = x2; x2 = y2; y2 = tmp;
        }

        if(x1 > x2) {
            int tmp = x1; x1 = x2; x2 = tmp;
            tmp     = y1; y1 = y2; y2 = tmp;
        }

        int dx = x2 - x1;
        int dy = y2 - y1;
        int y = y1;
        int eps = 0;

        for(int x = x1; x <= x2; ++x) {
            if(steep ) {
                callback(y, x);
            } else {
                callback(x, y);
            }
            eps += dy;
            if((eps << 1) >= dx) {
                ++y;
                eps -= dx;
            }
        }
    }
}
