﻿public enum LerpEquationTypes {
	Linear,
	SmoothCos,
	Quadratic,
	InverseQuadratic
}