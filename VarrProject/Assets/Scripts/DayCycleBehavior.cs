﻿using UnityEngine;
using System.Collections;



/// <summary>
/// Changes directional light and ambient light to simulate a day cicle.
/// A day cycle starts as midnight (-0.5). The sun rises at 6 (0.0), goes through
/// noon (0.5), and sets at 18 (1.0). Then the night goes till midnight (1.5).
/// </summary>
public class DayCycleBehavior : MonoBehaviour {
	public Light sunLight;
	public float simulationSpeed;

	private float m_CurrentTime;

	public enum DayStates { Day, Night }
	private DayStates m_CurrentDayState;
    
    public const float DayStart = -0.5f;
    public const float DayEnd   =  1.5f;
    public const float Noon     =  0.5f;
    public const float SunRise  =  0.0f;
    public const float SunSet   =  1.0f;

	public Color sunBorderColor;
	public Color sunMidBorderColor;
	public Color sunMidDayColor;
	public Color sunQuarterDayColor;

	public Color ambientBorderColor;
	public Color ambientMidBorderColor;
	public Color ambientQuarterDayColor;
	public Color ambientMidDayColor;

	public const float SunRiseTime            = 0.0f;
	public const float SunRisingTime          = 0.1f;
	public const float SunFirstMidQuarterTime = 0.25f;
	public const float SunMidDayTime          = 0.5f;
	public const float SunSecondQuarterTime   = 0.75f;
	public const float SunSettingTime         = 0.9f;
	public const float SunSetTime             = 1.0f;



	private void Update() {
        m_CurrentTime += Time.deltaTime * simulationSpeed;
        if(m_CurrentTime >= DayEnd) m_CurrentTime -= DayEnd;
        
        SetTime(m_CurrentTime);
	}
    
    
    
    private void SetTime(float time) {
        if(time < 0.0f || time > 1.0f) {
            //if(sunLight.gameObject.activeSelf) sunLight.gameObject.SetActive(false);
			m_CurrentTime = 0.0f;
        } else {
            if(!sunLight.gameObject.activeSelf) sunLight.gameObject.SetActive(true);
//            Vector3 lightAngle = new Vector3(0.0f, 270.0f, 0.0f);
//            lightAngle.x = Mathf.Lerp(0.0f, 180.0f, time);
//            sunLight.transform.eulerAngles = lightAngle;
			sunLight.transform.rotation = Quaternion.Euler(Mathf.Lerp(0.0f, 180.0f, time), 270.0f, 0.0f);

			sunLight.color = GetLightColor(time);
			RenderSettings.ambientLight = GetAmbientColor(time);
        }
    }



	public Color GetLightColor(float time) {
		Color firstColor = Color.magenta;
		Color secondColor = Color.magenta;
		float factor = 1.0f;

		if(time >= SunRiseTime && time < SunRisingTime) {
			firstColor = sunBorderColor;
			secondColor = sunMidBorderColor;
			factor = (time - SunRiseTime) / (SunRisingTime - SunRiseTime);
		} else if(time >= SunRisingTime && time < SunFirstMidQuarterTime) {
			firstColor = sunMidBorderColor;
			secondColor = sunQuarterDayColor;
			factor = (time - SunRisingTime) / (SunFirstMidQuarterTime - SunRisingTime);
		} else if(time >= SunFirstMidQuarterTime && time < SunMidDayTime) {
			firstColor = sunQuarterDayColor;
			secondColor = sunMidDayColor;
			factor = (time - SunFirstMidQuarterTime) / (SunMidDayTime - SunFirstMidQuarterTime);
		} else if(time >= SunMidDayTime && time < SunSecondQuarterTime) {
			firstColor = sunMidDayColor;
			secondColor = sunQuarterDayColor;
			factor = (time - SunMidDayTime) / (SunSecondQuarterTime - SunMidDayTime);
		} else if(time >= SunSecondQuarterTime && time < SunSettingTime) {
			firstColor = sunQuarterDayColor;
			secondColor = sunMidBorderColor;
			factor = (time - SunSecondQuarterTime) / (SunSettingTime - SunSecondQuarterTime);
		} else if(time >= SunSettingTime && time < SunSetTime) {
			firstColor = sunMidBorderColor;
			secondColor = sunBorderColor;
			factor = (time - SunSettingTime) / (SunSetTime - SunSettingTime);
		}

		LerpEquationTypes eType = LerpEquationTypes.SmoothCos;
		return(Color.Lerp(firstColor, secondColor, eType.GetEquationTransformedFactor(factor)));
	}



	public Color GetAmbientColor(float time) {
		Color firstColor = Color.magenta;
		Color secondColor = Color.magenta;
		float factor = 1.0f;
		
		if(time >= SunRiseTime && time < SunRisingTime) {
			firstColor = ambientBorderColor;
			secondColor = ambientMidBorderColor;
			factor = (time - SunRiseTime) / (SunRisingTime - SunRiseTime);
		} else if(time >= SunRisingTime && time < SunFirstMidQuarterTime) {
			firstColor = ambientMidBorderColor;
			secondColor = ambientQuarterDayColor;
			factor = (time - SunRisingTime) / (SunFirstMidQuarterTime - SunRisingTime);
		} else if(time >= SunFirstMidQuarterTime && time < SunMidDayTime) {
			firstColor = ambientQuarterDayColor;
			secondColor = ambientMidDayColor;
			factor = (time - SunFirstMidQuarterTime) / (SunMidDayTime - SunFirstMidQuarterTime);
		} else if(time >= SunMidDayTime && time < SunSecondQuarterTime) {
			firstColor = ambientMidDayColor;
			secondColor = ambientQuarterDayColor;
			factor = (time - SunMidDayTime) / (SunSecondQuarterTime - SunMidDayTime);
		} else if(time >= SunSecondQuarterTime && time < SunSettingTime) {
			firstColor = ambientQuarterDayColor;
			secondColor = ambientMidBorderColor;
			factor = (time - SunSecondQuarterTime) / (SunSettingTime - SunSecondQuarterTime);
		} else if(time >= SunSettingTime && time < SunSetTime) {
			firstColor = ambientMidBorderColor;
			secondColor = ambientBorderColor;
			factor = (time - SunSettingTime) / (SunSetTime - SunSettingTime);
		}
		
		LerpEquationTypes eType = LerpEquationTypes.SmoothCos;
		return(Color.Lerp(firstColor, secondColor, eType.GetEquationTransformedFactor(factor)));
	}
}
