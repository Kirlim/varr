﻿using UnityEngine;
using System.Collections;



/// <summary>
/// Simple movement that uses the current Pathfinder to
/// find and move through paths.
/// </summary>
public class MovementBehavior : MonoBehaviour {
	[SerializeField] protected PathPolicy pathPolicy;

	[SerializeField] protected float m_Speed;
	public float Speed { get { return(m_Speed); } }
	
	public bool AlignedHalt { get; protected set; } // Set this flag as true to force a unit to
	                                                // pause movement during a path, tile aligned
	public bool ForcedHalt { get; protected set; }
	public bool Halted { get { return(AlignedHalt || ForcedHalt); } }

	public bool Aligned { get; protected set; } // Is this component aligned with tiles?

	public IntVector2 Destination { get; protected set; }
	public MovementStates State { get; protected set; }

	protected IPath m_Path;
	protected IntVector2 m_NextTPos;
	protected static readonly IntVector2 m_NullPos = new IntVector2(-1, -1);

	protected Transform m_Transform;
	public const float MovDelta = 0.001f;



	protected void Awake() {
		m_Transform = this.transform;
		m_NextTPos = m_NullPos;
	}



	protected void Update() {
		if(!ForcedHalt && m_NextTPos != m_NullPos) {
			// Move to the next tile
			Vector3 pos = m_Transform.position;
            Vector3 dest = Global.Map.GetTileCenterPosition(m_NextTPos);
			
			pos.x = Mathf.MoveTowards(pos.x, dest.x, m_Speed * Time.deltaTime);
			pos.z = Mathf.MoveTowards(pos.z, dest.y, m_Speed * Time.deltaTime);

			// TODO : TO NOT WAST MOVEMENT DELTA IN THIS FRAME
			bool arrived = false;
			if(
				Mathf.Abs(pos.x - dest.x) < MovDelta &&
				Mathf.Abs(pos.z - dest.y) < MovDelta
			) arrived = true;

			// Get next tile, or finish the movement
			if(arrived) Aligned = true;
			if(Aligned && !AlignedHalt) {
				if(m_Path != null) {
					if(m_Path.Empty()) {
						State = MovementStates.Arrived;
						m_NextTPos = m_NullPos;
						m_Path = null;
					} else {
						m_NextTPos = m_Path.Pop();
						Aligned = false;
					}
				}
			}

			// Set updated position
			m_Transform.position = pos;
		}

        SynchToTerrainHeight();
	}



    public void SynchToTerrainHeight() {
        Vector3 pos = m_Transform.position;
        pos.y = Global.Map.GetTerrainHeight(pos.x, pos.z);
        m_Transform.position = pos;
    }



	public bool MoveTo(IntVector2 destination, bool canRepath = false) {
		if(
			canRepath == false && 
			State != MovementStates.Idle && State != MovementStates.Arrived
		) return(false);
		bool foundPath = false;

		UnityEngine.Profiling.Profiler.BeginSample("Pathfinding");
		IntVector2 current = 
			(m_NextTPos != m_NullPos) ? m_NextTPos : Global.Map.GetPosAsTile(m_Transform.position);
		m_Path = Global.PathFinder.GetPath(current, destination, pathPolicy);
		UnityEngine.Profiling.Profiler.EndSample();

		if(m_Path != null) {
			if(m_Path.Empty()) {
				State = MovementStates.Arrived;
			} else {
				State = MovementStates.Moving;
				m_NextTPos = m_Path.Pop();
				Aligned = false;
			}
			foundPath = true;
		}

		return(foundPath);
	}



	public void FinishMovement() {
		Assert.Test(State == MovementStates.Arrived);
		State = MovementStates.Idle;
	}

	

	public void RequestAlignedHalt() {
		AlignedHalt = true;
	}

	public void ReleaseAlignedHalt() {
		AlignedHalt = false;
	}



	public void ForceHalt() {
		ForcedHalt = true;
	}

	public void LiftForcedHalt() {
		ForcedHalt = false;
	}
}
