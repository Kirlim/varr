# Varr

Varr Reginlief was a game project inspired on Banished and Majesty. The game was on development until I figured out that it was too big to keep it as a side project (after I got a job), and then was put on halt.

Today, I know a lot more about games development and code writing. Surely, many things could be coded in a better architecture, and the code could be cleaner. But still, the source code is here as a portfolio, and reference to anyone who might be interested.

Made in Unity3D 5. Tested again with Unity3D 2017.3.

# How to play

Use the ScnTest scene. WSAD moves the camera, Q and E rotates. Left mouse button allows you to spawn objects on scene, and right mouse button spawns an almighty cubic slime that ignores light conditions.

Houses and Guard Stations need to be near roads to be valid.

Farmers will spawn as long as there is a valid house, a full grown farm and a Silo to put food. Be careful to not obstruct the silo door with buildings.

Guards will patrol and kill nearby slimes.

# Build for windows

A simple build has been uploaded to Dropbox:
https://www.dropbox.com/s/8wwbdi5n3vpo66h/VarrProject.zip?dl=0